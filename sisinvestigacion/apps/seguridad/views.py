from django.shortcuts import render,get_object_or_404,redirect
from django.http import HttpResponse
from django.contrib.auth.decorators import login_required
from django.contrib import auth
from django.contrib.auth import authenticate, login, logout
from django.conf import settings
from django.db.models import Count,Max, Q
from axes.models import AccessAttempt
from apps.paginacion import paginacion
from apps.reportes.models import historial
from .forms import  formPerfil, LoginForm, formUsuario, formModulo,formSubModulo,formEditUsuario,formFoto, formgaleria, formfoto
from .models import modulos, permisos, perfil, User, galeria, fotos
from apps.paginacion import paginacion

import datetime, time
today  = datetime.datetime.now()
fecha  = today.strftime("%Y-%m-%d")
hora   = time.strftime("%H:%M:%S") 

#from django.utils.decorators import method_decorator

# Crea tus vista aqui.
def historiales(request,mod):
    ip        = request.META['REMOTE_ADDR']
    equipo    = request.META['HTTP_USER_AGENT']
    a = historial()
    a.idusuario_id  = request.user.id
    a.fecha         = fecha
    a.hora          = hora
    a.equipo        = equipo
    a.ip            = ip 
    a.modulo        = mod[0]
    a.accion        = mod[1]
    a.idaccion      = mod[2]
    a.save()


def indexweb(request):
    g = galeria.objects.filter(estado= True).order_by('-id')  
    f = fotos.objects.select_related('idgaleria').filter(estado=True).values('url','titulo','descripcion','idgaleria__clase').order_by('-id')  
    m = {'galeria':g,'fotos':f}
    return render(request,'index/index.html',m)


@login_required(login_url='/')
def galerias(request):
    estados =  permi(request, "registro_galeria")
    if request.method == 'POST': 
        formuP = formgaleria(request.POST,request.FILES )
        if formuP.is_valid():
            try:
                idg = galeria.objects.latest('id')
                idg = idg.id
            except galeria.DoesNotExist:
                idg = 0

            g        = galeria()
            g.id     = idg + 1 
            g.nombre = request.POST["nombre"]
            g.estado = True
            g.save()            

            idg= galeria.objects.latest('id')
            historiales(request,["galeria","registrar",idg.id])
        else:
            return render(request,'galeria/form_gl.html',{'formu':formuP})
        
        g = galeria.objects.filter(estado= True).order_by('-id')  
        modulo = {'lista':g,'url':'registro_galeria/','n':'fgaleriaU','estado':estados}
        return render(request,'galeria/ajax_galeria.html',modulo)            
    else:
        g = galeria.objects.filter(estado= True).order_by('-id')
        formu = formgaleria()
        modulo = {'lista':g,'formu':formu, 'url':'registro_galeria/','n':'fgaleriaU','estado':estados}
        return render(request,'galeria/galeria.html',modulo)        

@login_required(login_url='/')
def actualizar_galeria(request):
    g = galeria.objects.filter(estado= True).order_by('-id')
    estado =  permi(request, "registro_galeria")
    if request.method == 'POST' and request.is_ajax(): 
        idp = request.POST.get("id","")
        a=get_object_or_404(galeria,pk=idp)
        form=formgaleria(request.POST, instance=a)
        if form.is_valid():
            form.save()
            historiales(request,["galeria","actualizar",idp])
            return render(request,'galeria/ajax_galeria.html',{'lista':g,'n':'fgaleriaU','estado':estado}) 
        else:
            return render(request,'galeria/form_gl.html',{'formu':form})             
    else:
        idp = request.GET.get("id","")
        a=get_object_or_404(galeria,pk=idp)
        form= formgaleria(instance=a)
        return render(request,'seguridad/modal.html',{'nombre':form,'url':'actualizar_galeria/','n':'fgaleriaU','u':'fgaleriaU','estado':estado}) 

@login_required(login_url='/')
def eliminar_galeria(request):
    g = galeria.objects.filter(estado= True).order_by('-id')
    estado =  permi(request, "registro_galeria")
    if request.method == 'GET' and request.is_ajax(): 
        idb = request.GET.get("id","")
        a= galeria.objects.get(pk=idb)
        a.estado = False
        a.save()    
        historiales(request,["galeria","eliminar",idb])
        return render(request,'galeria/ajax_galeria.html',{'lista':g,'n':'fgaleriaU','estado':estado})     

@login_required(login_url='/')
def registro_fotos(request):
    estados =  permi(request, "registro_galeria")
    print(estados)
    if request.method == 'POST': 
        formuP = formfoto(request.POST,request.FILES )
        if formuP.is_valid():
            formuP.save()          
            idg= fotos.objects.latest('id')
            historiales(request,["fotos","registrar",idg.id])
        else:
            return render(request,'fotos/form_fo.html',{'formuH':formuP})
        
        g = fotos.objects.filter(estado= True, idgaleria = request.POST["idg"]).order_by('-id')  
        modulo = {'lista':g,'url':'registro_fotos/','n':'ffotosU','estado':estados}
        return render(request,'fotos/ajax_fotos.html',modulo)            
    else:
        g  = fotos.objects.filter(estado= True, idgaleria = request.GET["idgaleria"]).order_by('-id')
        ng = galeria.objects.values('nombre').get(pk= request.GET["idgaleria"])
        
        formu = formfoto()
        modulo = {'lista':g,'formuH':formu, 'url':'registro_fotos/','n':'ffotosU','nd':ng['nombre'],'idg':request.GET["idgaleria"],'estado':estados}
        return render(request,'fotos/fotos.html',modulo)        

@login_required(login_url='/')
def actualizar_fotos(request):
    estado =  permi(request, "registro_galeria")
    if request.method == 'POST' and request.is_ajax(): 
        idp = request.POST.get("id","")
        a=get_object_or_404(fotos,pk=idp)
        form=formfoto(request.POST, instance=a)
        g  = fotos.objects.filter(estado= True, idgaleria = request.POST["idg"]).order_by('-id')
        if form.is_valid():
            form.save()
            historiales(request,["fotos","actualizar",idp])
            return render(request,'fotos/ajax_fotos.html',{'idg':request.POST["idg"],'lista':g,'n':'ffotosU','estado':estado}) 
        else:
            return render(request,'fotos/form_fo.html',{'formu':form})             
    else:
        idp = request.GET.get("id","")
        a=get_object_or_404(fotos,pk=idp)
        form= formfoto(instance=a)
        return render(request,'fotos/modal.html',{'idg':request.GET["idg"],'formuH':form,'url':'actualizar_fotos/','n':'ffotosU','u':'ffotosU','estado':estado}) 

@login_required(login_url='/')
def eliminar_fotos(request):
    g  = fotos.objects.filter(estado= True, idgaleria = request.GET["idg"]).order_by('-id')
    estado =  permi(request, "registro_galeria")
    if request.method == 'GET' and request.is_ajax(): 
        idb = request.GET.get("id","")
        a= fotos.objects.get(pk=idb)
        a.estado = False
        a.save()    
        historiales(request,["fotos","eliminar",idb])
        return render(request,'fotos/ajax_fotos.html',{'lista':g,'n':'ffotosU','estado':estado})     


def Login(request):
    u = request.user
    if u.is_anonymous():
        if request.method == 'POST':
            formulario = LoginForm(request.POST)

            if formulario.is_valid():
                username  = formulario.cleaned_data['username']
                password = formulario.cleaned_data['password']

                user = authenticate(username=username, password=password)
                if user is not None:
                    if user.is_active:
                        auth.login(request, user)
                        return redirect("/sistema")
                    else: 
                        msm= "cuenta desactivada"  

            msm = "DATOS INCORRECTOS"
            login = LoginForm()
            return render(request,'seguridad/cuentas/login.html',{'login':login,'msm':msm})
        else:
            login = LoginForm()
            return render(request,'seguridad/cuentas/login.html',{'login':login,'msm':''})        

    else:
        return redirect('/sistema')


def LogOut(request):
    logout(request)
    return redirect('/')

@login_required(login_url='/')
def index(request):
    idp = request.user.idperfil_id
    mod = permisos.objects.filter(idperfil_id=idp, idmodulo__estado=True).values('idmodulo_id','idmodulo__padre','idmodulo__descripcion','idmodulo__icon','idmodulo__url','idperfil_id','buscar','eliminar','editar','insertar','imprimir','ver') 
    return render(request,'seguridad/index.html',{'mod':mod})

def permi(request,url):
    idp = request.user.idperfil_id
    mod = permisos.objects.filter(idmodulo__url=url, idperfil_id=idp, idmodulo__estado=True).values('idmodulo__url','buscar','eliminar','editar','insertar','imprimir','ver')
    return mod

@login_required(login_url='/')
def registrar_perfil(request):
    perfiles = perfil.objects.all().order_by('id')
    estado =  permi(request, "registro_perfil")
    if request.method == 'POST' and request.is_ajax(): 
        formu = formPerfil(request.POST)
        listaMod = [(con.id) for con in modulos.objects.all()]
        if formu.is_valid():
            formu.save()
            idp = perfil.objects.latest('id')
            for x in listaMod:
                m  = permisos()
                m.idmodulo_id = x
                m.idperfil_id = idp.id
                m.save() 
            historiales(request,["perfil","registrar",idp.id])                
            return render(request,'seguridad/perfil/ajax_perfil.html',{'perfil':perfiles,'n':'perfilU','estado':estado})            
        else:
            return render(request,'seguridad/perfil/form_per.html',{'formu':formu})         
    else:
        formu = formPerfil()
        return render(request,'seguridad/perfil/perfil.html',{'formu':formu,'perfil':perfiles, 'url':'registro_perfil/','n':'perfilU','estado':estado})

@login_required(login_url='/')
def eliminar_perfil(request):
    perfiles = perfil.objects.all().order_by('id')
    estado =  permi(request, "registro_perfil")
    if request.method == 'GET' and request.is_ajax(): 
        idb = request.GET.get("id","")
        historiales(request,["perfil","eliminar",idb])
        get_object_or_404(perfil,pk=idb).delete()          
        permisos.objects.filter(idperfil=idb).delete()          
        return render(request,'seguridad/perfil/ajax_perfil.html',{'perfil':perfiles,'n':'perfilU','estado':estado})     

@login_required(login_url='/')
def actualizar_perfil(request):
    perfiles = perfil.objects.all().order_by('id')
    estado =  permi(request, "registro_perfil")
    if request.method == 'POST' and request.is_ajax(): 
        idp = request.POST.get("id","")
        a=get_object_or_404(perfil,pk=idp)
        form=formPerfil(request.POST, instance=a)
        if form.is_valid():
            form.save()
            historiales(request,["perfil","actualizar",idp])
            return render(request,'seguridad/perfil/ajax_perfil.html',{'perfil':perfiles,'n':'perfilU','estado':estado}) 
        else:
            return render(request,'seguridad/perfil/form_per.html',{'formu':form})             
    else:
        idp = request.GET.get("id","")
        a=get_object_or_404(perfil,pk=idp)
        form= formPerfil(instance=a)
        return render(request,'seguridad/modal.html',{'nombre':form,'url':'actualizar_perfil/','n':'perfilU','u':'perfilU','estado':estado}) 

@login_required(login_url='/')
def registro_usuario(request):
    usuarios = User.objects.all().order_by('id')
    estado =  permi(request, "registro_usuario")
    if request.method == 'POST' and request.is_ajax(): 
        formu = formUsuario(request.POST)
        if formu.is_valid():
            formu.save()
            idp = User.objects.latest('id')
            historiales(request,["usuario","registrar",idp.id])
            return render(request,'seguridad/usuario/ajax_usuario.html',{'usuario':usuarios,'n':'UserU','estado':estado})            
        else:
            return render(request,'seguridad/usuario/form_user.html',{'formu':formu})
        
    else:
        estado =  (permi(request, "registro_usuario"))
        formu = formUsuario()
        return render(request,'seguridad/usuario/usuario.html',{'formu':formu,'usuario':usuarios, 'url':'registro_usuario/','n':'UserU','estado':estado})

@login_required(login_url='/')
def passDefault(request):
    idp = request.GET.get("id","")
    estado =  (permi(request, "registro_usuario"))
    u = User.objects.get(pk=idp)
    u.password = "pbkdf2_sha256$20000$63ijtCdaGIEx$Wcfn0iEAQfno+SMy1v1ttxd6WQZXyAkdjmOacuNHm/4="
    u.save()
    historiales(request,["usuario","cambio contraseña",idp])
    usuarios = User.objects.all().order_by('id')
    return render(request,'seguridad/usuario/ajax_usuario.html',{'usuario':usuarios,'n':'UserU','estado':estado})            


@login_required(login_url='/')
def eliminar_usuario(request):
    usuarios = User.objects.all().order_by('id')
    estado =  permi(request, "registro_usuario")
    if request.method == 'GET' and request.is_ajax(): 
        idb = request.GET.get("id","")
        a= User.objects.get(pk=idb)
        a.estado = False
        a.save()    
        historiales(request,["usuario","eliminar",idb])
        return render(request,'seguridad/usuario/ajax_usuario.html',{'usuario':usuarios,'n':'UserU','estado':estado})     

@login_required(login_url='/')
def actualizar_usuario(request):
    usuarios = User.objects.all().order_by('id')
    estado =  permi(request, "registro_usuario")
    if request.method == 'POST' and request.is_ajax(): 
        idp = request.POST.get("id","")
        a=get_object_or_404(User,pk=idp)
        form=formUsuario(request.POST, instance=a)
        if form.is_valid():
            form.save()
            historiales(request,["usuario","actualizar",idp])
            return render(request,'seguridad/usuario/ajax_usuario.html',{'usuario':usuarios,'n':'UserU','estado':estado}) 
        else:
            return render(request,'seguridad/usuario/form_user.html',{'formu':form})            
    else:
        idp = request.GET.get("id","")
        a=get_object_or_404(User,pk=idp)
        form= formUsuario(instance=a)
        return render(request,'seguridad/modal.html',{'nombre':form,'url':'actualizar_usuario/','n':'UserU','u':'UserU','estado':estado}) 

@login_required(login_url='/')
def actualizar_info_usuario(request):
    usuarios = User.objects.all().order_by('id')
    if request.method == 'POST' and request.is_ajax(): 
        idp = request.user.id
        a=get_object_or_404(User,pk=idp)
        form=formEditUsuario(request.POST, instance=a)
        if form.is_valid():
            form.save()
            historiales(request,["usuario","actualizar perfil",idp])
            return redirect('/') 
        else:
            return render(request,'seguridad/usuario/form_user.html',{'formu':form})             
    else:
        idp = request.user.id
        a=get_object_or_404(User,pk=idp)
        form= formEditUsuario(instance=a)
        return render(request,'seguridad/usuario/edit_info_user.html',{'form':form}) 


@login_required(login_url='/')
def profile(request):
    return render(request,'seguridad/usuario/cuenta.html',{'f':'ff'}) 

@login_required(login_url='/')
def fotoperfil(request):
    a=get_object_or_404(User,pk=request.user.id)
    formu = formFoto(request.POST,request.FILES, instance=a )
    if formu.is_valid():
        formu.save()
    return HttpResponse("ok")

@login_required(login_url='/')
def registro_modulo(request):
    modulo = modulos.objects.filter(estado=True).order_by('id')
    estado =  permi(request, "registro_modulo")
    if request.method == 'POST' and request.is_ajax(): 
        formu = formModulo(request.POST)
        if formu.is_valid():
            formu.save()
            listaPer = [(con.id) for con in perfil.objects.all()]
            idm = modulos.objects.latest('id')
            for x in listaPer:
                m  = permisos()
                m.idmodulo_id = idm.id
                m.idperfil_id = x
                m.save() 
            idp = modulos.objects.latest('id')
            historiales(request,["modulo","registrar",idp.id])
            return render(request,'seguridad/modulo/ajax_modulo.html',{'modulo':modulo,'n':'ModuloU','estado':estado})            
        else:
            return render(request,'seguridad/modulo/form_modulo.html',{'formu':formu})                        
    else:
        formu = formModulo()
        formu2 = formSubModulo()
        return render(request,'seguridad/modulo/modulo.html',{'pa':'1','formu':formu,'formu2':formu2,'modulo':modulo, 'url':'registro_modulo/','n':'ModuloU','nm':'SubModuloU','estado':estado})



@login_required(login_url='/')
def eliminar_modulo(request):
    modulo = modulos.objects.filter(estado=True).order_by('id')
    estado =  permi(request, "registro_modulo")
    if request.method == 'GET' and request.is_ajax(): 
        idb = request.GET.get("id","")
        a= modulos.objects.get(pk=idb)
        a.estado = False
        a.save()  
        #get_object_or_404(modulos,pk=idb).delete()            
        historiales(request,["modulo","eliminar",idb])        
        return render(request,'seguridad/modulo/ajax_modulo.html',{'modulo':modulo,'n':'ModuloU','estado':estado})     

@login_required(login_url='/')
def actualizar_modulo(request):
    modulo = modulos.objects.filter(estado=True).order_by('id')
    estado =  permi(request, "registro_modulo")
    if request.method == 'POST' and request.is_ajax(): 
        idp = request.POST.get("id","")
        a=get_object_or_404(modulos,pk=idp)
        form=formModulo(request.POST, instance=a)
        if form.is_valid():
            form.save()            
            historiales(request,["modulo","actualizar",idp])            
            return render(request,'seguridad/modulo/ajax_modulo.html',{'modulo':modulo,'n':'ModuloU','estado':estado}) 
        else:
            return render(request,'seguridad/modulo/form_modulo.html',{'formu':form})               
    else:
        idp = request.GET.get("id","")
        a=get_object_or_404(modulos,pk=idp)
        form= formModulo(instance=a)
        return render(request,'seguridad/modal.html',{'nombre':form,'url':'actualizar_modulo/','n':'ModuloU','u':'ModuloU','estado':estado}) 


@login_required(login_url='/')
def eliminar_submodulo(request):
    modulo = modulos.objects.filter(estado=True).order_by('id')
    estado =  permi(request, "registro_modulo")
    if request.method == 'GET' and request.is_ajax(): 
        idb = request.GET.get("id","")
        
        for i in modulos.objects.filter(pk=idb):
            padre = i.padre 

        a= modulos.objects.get(pk=idb)
        a.estado = False
        a.save() 
       # get_object_or_404(modulos,pk=idb).delete()                    
        historiales(request,["submodulo","eliminar",idb])
        return render(request,'seguridad/modulo/ajax_submodulo.html',{'modulo':modulo,'nm':'SubModuloU','padre':str(padre),'estado':estado})     

@login_required(login_url='/')
def actualizar_submodulo(request):
    modulo = modulos.objects.filter(estado=True).order_by('id')
    estado =  permi(request, "registro_modulo")
    if request.method == 'POST' and request.is_ajax(): 
        idp = request.POST.get("id","")
        a=get_object_or_404(modulos,pk=idp)
        for i in modulos.objects.filter(pk=idp):
            padre = i.padre        
        form=formSubModulo(request.POST, instance=a)
        if form.is_valid():
            form.save()            
            historiales(request,["submodulo","actualizar",idp])            
            return render(request,'seguridad/modulo/ajax_submodulo.html',{'padre':str(padre),'modulo':modulo,'nm':'SubModuloU','estado':estado}) 
    else:
        idp = request.GET.get("id","")
        a=get_object_or_404(modulos,pk=idp)
        form= formSubModulo(instance=a)
        return render(request,'seguridad/modal.html',{'nombre':form,'url':'actualizar_submodulo/','n':'SubModuloU','u':'SubModuloU','estado':estado}) 

@login_required(login_url='/')
def registro_submodulo(request):
    estado =  permi(request, "registro_modulo")
    modulo = modulos.objects.filter(estado=True).order_by('id')
    if request.method == 'POST' and request.is_ajax(): 
        formu = formSubModulo(request.POST)
        padre = request.POST.get("padre","")
        if formu.is_valid():
            formu.save()
            idm = modulos.objects.latest('id')
            listaPer = [(con.id) for con in perfil.objects.all()]
            for x in listaPer:
                m  = permisos()
                m.idmodulo_id = idm.id
                m.idperfil_id = x
                m.save()             
            idp = modulos.objects.latest('id')
            historiales(request,["submodulo","registrar",idp.id])            
            return render(request,'seguridad/modulo/ajax_submodulo.html',{'modulo':modulo,'nm':'SubModuloU','padre':padre,'estado':estado}) 
        else:
            return render(request,'seguridad/modulo/form_submodulo.html',{'formu':formu})           
    else:
        idp = request.GET.get("id","")
        modulo = modulos.objects.filter(padre=idp, estado=True)
        #print(modulo.query) #imprime las consultas en el terminal
        return render(request,'seguridad/modulo/ajax_submodulo.html',{'modulo':modulo,'nm':'SubModuloU','padre':idp,'estado':estado}) 


@login_required(login_url='/')
def registro_permisos(request):
    
    if request.method == 'POST' and request.is_ajax():
        idb = request.POST.get("id","")
        permisos.objects.select_related('idmodulo').filter(idperfil_id=idb, idmodulo__estado=True).values('id','idmodulo_id','idmodulo__padre','idmodulo__descripcion','idperfil_id','buscar','eliminar','editar','insertar','imprimir','ver')
        return render(request,'seguridad/permisos/ajax_permisos.html',{'permisos':permiso})
    else:
        idb = 2

    permiso = permisos.objects.select_related('idmodulo').filter(idperfil_id=idb, idmodulo__estado=True).values('id','idmodulo_id','idmodulo__padre','idmodulo__descripcion','idperfil_id','buscar','eliminar','editar','insertar','imprimir','ver')    
    permiso1 = permisos.objects.values('idperfil__descripcion','idperfil_id').annotate(Count('idperfil'))
    #print(permiso.query)
    return render(request,'seguridad/permisos/permisos.html',{'permisos':permiso, 'permisos1':permiso1,'url':'registro_permisos/','n':'PermisosU'})


@login_required(login_url='/')
def cambiarEstadoPermiso(request):
    if request.method == 'GET' and request.is_ajax(): 
        idp = request.GET.get("id","")
        u = request.GET.get("url","")
        e = request.GET.get("e","")
        if e == 'true':
            e= False
        else:
            e= True

        a= permisos.objects.get(pk=idp)

        if (u == "v"):
            a.ver = e
        if (u == "e"):
            a.editar = e
        elif (u == "b"):
            a.buscar = e
        elif (u == "i"):
            a.insertar = e            
        elif (u == "el"):
            a.eliminar = e            
        elif (u == "im"):
            a.imprimir = e            

        a.save()        
        historiales(request,["permisos","modificar",idp])
        return HttpResponse('ok')

@login_required(login_url='/')
def cambiarEstadoPermiso2(request):
    if request.method == 'GET' and request.is_ajax(): 
        idp = request.GET.get("id","")
        e = request.GET.get("e","")
        if e == 'true':
            e= False
        else:
            e= True

        a= permisos.objects.get(pk=idp)

        a.ver = e
        a.editar = e
        a.buscar = e
        a.insertar = e            
        a.eliminar = e            
        a.imprimir = e            

        a.save()
        historiales(request,["permisos","modificar",idp])

        return HttpResponse('ok')

@login_required(login_url='/')
def user_block(request):
    userBlock = AccessAttempt.objects.all()
    estado =  permi(request, "registro_modulo")
    if request.method == 'POST' and request.is_ajax(): 
        idp = request.POST.get("id","")
        u = AccessAttempt.objects.get(pk=idp)
        u.failures_since_start = request.POST["ni"]
        u.save()        
        historiales(request,["userBlock","modificar",idp])        
        return render(request,'seguridad/userBlock/ajax_user_block.html',{'lista':userBlock,'estado':estado}) 
    else:
        modulo = {'estado':estado,'url':'user_block/'}
        return paginacion(request,userBlock, modulo, 'seguridad/userBlock/user_block.html' )         

def busq_ajax_us(request):
    dat = request.GET.get('datos')
    estado =  permi(request, "user_block")
    if request.GET.get('d') == "v":
        e = AccessAttempt.objects.filter( Q(ip_address__contains=dat))[:10]
    elif request.GET.get('d') == "b":
        e = AccessAttempt.objects.filter(failures_since_start=dat)[:10]           

    modulo = {'lista':e,'estado':estado}
    return render(request,'seguridad/userBlock/ajax_user_block.html', modulo)


def manual(request):
    return render(request,'seguridad/manual/manual.html')