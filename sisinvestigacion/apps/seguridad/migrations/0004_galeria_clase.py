# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('seguridad', '0003_fotos_estado'),
    ]

    operations = [
        migrations.AddField(
            model_name='galeria',
            name='clase',
            field=models.CharField(max_length=50, default=1),
            preserve_default=False,
        ),
    ]
