from django import forms
from django.forms import ModelForm
from apps.matenimiento.models import alumno, docente, semestre_academico, estado, oficina, administrativo, externo
from .models import proyecto, solicitud, informe, resolucion
import datetime
f  = datetime.datetime.now()
fechaactual   = f.strftime("%Y-%m-%d")  


idSemestre = semestre_academico.objects.latest('id')
class formproyecto(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(formproyecto, self).__init__(*args, **kwargs)
        self.fields['documento'].widget = forms.Textarea(attrs={'class':'form-control input-sm', 'required':'','rows':5,'placeholder':'código y nombre proyecto'})
        self.fields['fecharecepcion'].widget = forms.HiddenInput(attrs={'value':fechaactual})
        self.fields['fechainicio'].widget = forms.TextInput(attrs={'class':'form-control input-sm', 'type':'date' ,'required':'','placeholder':'Día/Mes/Año'})
        self.fields['fechatermino'].widget = forms.TextInput(attrs={'class':'form-control input-sm', 'type':'date' ,'required':'','placeholder':'Día/Mes/Año'})
        self.fields['asunto'].widget = forms.Textarea(attrs={'class':'form-control input-sm', 'required':'','rows':5})
        self.fields['archivo'].widget = forms.ClearableFileInput(attrs={'required':''})
        self.fields['idestado'].widget = forms.HiddenInput( attrs={'value':1})
        self.fields['idsemestre'].widget = forms.HiddenInput( attrs={'value':idSemestre.id})

    class Meta:
        model = proyecto
        exclude = ['proveido','fechaemision',"estado",'idresolucion','idinforme','observacion', 'idsolicitud', 'iddocente', 'idalumno','idexterno','idadministrativo']        

class formSolicitud(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(formSolicitud, self).__init__(*args, **kwargs)
        self.fields['nombresolicitud'].widget = forms.TextInput(attrs={'class':'form-control input-sm', 'required':''})
        self.fields['proveidosolicitud'].widget = forms.TextInput(attrs={'class':'form-control input-sm', 'required':''})
        self.fields['fechadocumento'].widget = forms.TextInput(attrs={'class':'form-control input-sm', 'required':'', 'type':'date'})
        self.fields['asuntosolicitud'].widget = forms.Textarea(attrs={'class':'form-control input-sm', 'required':'','rows':5})
        self.fields['archivosolicitud'].widget = forms.ClearableFileInput(attrs={'class':'form-control input-sm', 'required':''})
        

    class Meta:
        model = solicitud
        exclude = ["estado"]  

listaOficinas = [(con.id, con.oficina) for con in oficina.objects.filter(estadosistema= True)]
class formInforme(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(formInforme, self).__init__(*args, **kwargs)
        self.fields['numeroinforme'].widget = forms.TextInput(attrs={'class':'form-control input-sm', 'required':''})
        self.fields['proveidoinforme'].widget = forms.TextInput(attrs={'class':'form-control input-sm', 'required':''})
        self.fields['fechaenvio'].widget = forms.TextInput(attrs={'class':'form-control input-sm', 'required':'', 'type':'date','value':fechaactual})
        self.fields['fechadocumento'].widget = forms.TextInput(attrs={'class':'form-control input-sm', 'required':'', 'type':'date'})
        self.fields['asuntoinforme'].widget = forms.Textarea(attrs={'class':'form-control input-sm', 'required':'','rows':5})
        self.fields['archivoinforme'].widget = forms.ClearableFileInput(attrs={'class':'form-control input-sm', 'required':''})
        self.fields['idoficina'].widget = forms.Select( choices=listaOficinas,attrs={'class':'form-control chosen-select'})
        

    class Meta:
        model = informe
        exclude = ["estado"]  

listaOficinas = [(con.id, con.oficina) for con in oficina.objects.filter(estadosistema= True)]
class formResolucion(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(formResolucion, self).__init__(*args, **kwargs)
        self.fields['numeroresolucion'].widget = forms.TextInput(attrs={'class':'form-control input-sm', 'required':''})
        self.fields['proveidoresolucion'].widget = forms.TextInput(attrs={'class':'form-control input-sm', 'required':''})
        self.fields['fechaenvio'].widget = forms.TextInput(attrs={'class':'form-control input-sm', 'required':'', 'type':'date','value':fechaactual})
        self.fields['fechadocumento'].widget = forms.TextInput(attrs={'class':'form-control input-sm', 'required':'', 'type':'date'})
        self.fields['asuntoresolucion'].widget = forms.Textarea(attrs={'class':'form-control input-sm', 'required':'','rows':5})
        self.fields['archivoresolucion'].widget = forms.ClearableFileInput(attrs={'class':'form-control input-sm', 'required':''})
        self.fields['idoficina'].widget = forms.Select( choices=listaOficinas,attrs={'class':'form-control chosen-select'})        

    class Meta:
        model = resolucion
        exclude = ["estado"]  


class formDocente(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(formDocente, self).__init__(*args, **kwargs)
        self.fields['nombre'].widget = forms.TextInput(attrs={'class':'form-control input-sm', 'required':''})
        self.fields['apellido'].widget = forms.TextInput(attrs={'class':'form-control input-sm', 'required':''})
        self.fields['codigo'].widget = forms.TextInput(attrs={'class':'form-control input-sm', 'required':''})

    class Meta:
        model = docente
        exclude = ["estadosistema",'dni','correo']  

class formAlumno(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(formAlumno, self).__init__(*args, **kwargs)
        self.fields['nombre'].widget = forms.TextInput(attrs={'class':'form-control input-sm', 'required':''})
        self.fields['apellido'].widget = forms.TextInput(attrs={'class':'form-control input-sm', 'required':''})
        self.fields['codigo'].widget = forms.TextInput(attrs={'class':'form-control input-sm', 'required':''})

    class Meta:
        model = alumno
        exclude = ["estadosistema"]          

class formAdministrativo(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(formAdministrativo, self).__init__(*args, **kwargs)
        self.fields['nombre'].widget = forms.TextInput(attrs={'class':'form-control input-sm', 'required':''})
        self.fields['apellido'].widget = forms.TextInput(attrs={'class':'form-control input-sm', 'required':''})
        self.fields['codigo'].widget = forms.TextInput(attrs={'class':'form-control input-sm', 'required':''})

    class Meta:
        model = administrativo
        exclude = ["estadosistema",'dni','correo','celular','ocupacion','idoficina'] 

class formExterno(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(formExterno, self).__init__(*args, **kwargs)
        self.fields['nombre'].widget = forms.TextInput(attrs={'class':'form-control input-sm', 'required':''})
        self.fields['apellido'].widget = forms.TextInput(attrs={'class':'form-control input-sm', 'required':''})
        self.fields['codigo'].widget = forms.TextInput(attrs={'class':'form-control input-sm', 'required':''})

    class Meta:
        model = externo
        exclude = ["estadosistema",'dni','formacionacademica','celular','ocupacion','centrolabores'] 