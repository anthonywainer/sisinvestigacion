from django.db import models
from apps.matenimiento.models import modalidad_participante, oficina, alumno, docente, semestre_academico, estado, administrativo, externo


class solicitud(models.Model):
    nombresolicitud   = models.CharField(max_length=100)
    asuntosolicitud   = models.CharField(max_length=300)
    proveidosolicitud = models.CharField(max_length=200)
    fechadocumento    = models.DateField()
    archivosolicitud  = models.FileField(upload_to='Solicitud/%Y/%m/%d')
    estado            = models.BooleanField(default=True)

class informe(models.Model):
    numeroinforme   = models.CharField(max_length=100)
    asuntoinforme   = models.CharField(max_length=300)
    proveidoinforme = models.CharField(max_length=300)
    fechadocumento  = models.DateField()
    fechaenvio      = models.DateField()
    archivoinforme  = models.FileField(upload_to='Informe/%Y/%m/%d')
    idoficina       = models.ForeignKey(oficina,null=True)
    estado          = models.BooleanField(default=True)

class resolucion(models.Model):
    numeroresolucion    = models.CharField(max_length=20)
    asuntoresolucion    = models.CharField(max_length=300)
    proveidoresolucion  = models.CharField(max_length=200)
    fechadocumento      = models.DateField()
    #fecharecepcion      = models.DateField()
    fechaenvio          = models.DateField()
    archivoresolucion   = models.FileField(upload_to='Resolucion/%Y/%m/%d')
    idoficina           = models.ForeignKey(oficina,null=True) 
    estado              = models.BooleanField(default=True)



class proyecto(models.Model):
    documento       = models.CharField(max_length=200)
    fecharecepcion  = models.DateField()
    fechaemision    = models.DateField(null=True)
    fechainicio     = models.DateField()
    fechatermino    = models.DateField()
    asunto          = models.TextField(max_length=500)
    proveido        = models.CharField(max_length=200,null=True)
    archivo         = models.FileField(upload_to='Proyecto/%Y/%m/%d')
    observacion     = models.TextField(max_length=500,null=True)
    idsemestre      = models.ForeignKey(semestre_academico)
    idsolicitud     = models.ForeignKey(solicitud,null=True)
    idinforme       = models.ForeignKey(informe,null=True)
    idresolucion    = models.ForeignKey(resolucion,null=True)
    idestado        = models.ForeignKey(estado)
    idalumno        = models.ManyToManyField(alumno, through='proyeAl')
    idadministrativo = models.ManyToManyField(administrativo, through='proyeAd')
    idexterno        = models.ManyToManyField(externo, through='proyeEx')
    iddocente       = models.ManyToManyField(docente, through='proyeDo')
    estado          = models.BooleanField(default=True)

class proyeAl(models.Model):
    idalumno         = models.ForeignKey(alumno)
    idproyectoalumno = models.ForeignKey(proyecto)
    modalidadalumno  = models.ForeignKey(modalidad_participante)


class proyeDo(models.Model):
    iddocente         = models.ForeignKey(docente)
    idproyectodocente = models.ForeignKey(proyecto)
    modalidaddocente  = models.ForeignKey(modalidad_participante)

class proyeEx(models.Model):
    idpexterno  = models.ForeignKey(externo)
    idproyectoexterno = models.ForeignKey(proyecto)
    modalidadexterno = models.ForeignKey(modalidad_participante)

class proyeAd(models.Model):
    idadministrativo = models.ForeignKey(administrativo)
    idproyectoadministrativo = models.ForeignKey(proyecto)
    modalidadadministrativo  = models.ForeignKey(modalidad_participante)