from django.conf.urls import patterns, url, include
from .views import *

urlpatterns = [
	#este es mi primer index
    url(r'^registro_proyecto/$', registro_proyecto),
    url(r'^actualizar_proyecto/$', actualizar_proyecto),
    url(r'^eliminar_proyecto/$', eliminar_proyecto),
    url(r'^cambiarestado$', cambiarestado),
    url(r'^busq_ajax_pro/$', busq_ajax_pro),
    
    url(r'^traerparticipantes$', traerparticipantes),
    url(r'^mostrarparticipante$', mostrarparticipante),
    url(r'^actualizarparticipante$', actualizarparticipante),
    url(r'^mostrarproyecto$', mostrarproyecto),

    #agregar docente
    url(r'^adocente$', Adocente),

    #agregar docente
    url(r'^aalumno$', Aalumno),

    #agregar docente
    url(r'^aadministrativo$', Aadministrativo),

    #agregar docente
    url(r'^aexterno$', Aexterno),

    #solicitud
    url(r'^mostrarsolicitud$', mostrarsolicitud),    
    url(r'^editarsolicitud$', editarsolicitud),       

    #informe
    url(r'^registro_informe/$', registro_informe),  
    url(r'^mostrarinforme$', mostrarinforme),  
    url(r'^editarinforme$', editarinforme),      

    #resolucion
    url(r'^registro_resolucion/$', registro_resolucion),  
    url(r'^mostrarresolucion$', mostrarresolucion),  
    url(r'^editarresolucion$', editarresolucion),      


]