from django.shortcuts import render,get_object_or_404,redirect
from django.http import HttpResponse
from django.contrib.auth.decorators import login_required
from django.db.models import Max, Q
from django.http import JsonResponse
from apps.seguridad.models import permisos
from apps.reportes.models import historial
from apps.matenimiento.models import alumno, docente, semestre_academico, estado, modalidad_participante, oficina, administrativo, externo
from .forms import formproyecto, formSolicitud, formInforme, formResolucion, formDocente, formAlumno, formAdministrativo, formExterno
from .models import proyecto, solicitud, proyeAl, proyeDo, proyeAd,proyeEx, informe, resolucion
from apps.paginacion import paginacion
import datetime, time
today  = datetime.datetime.now()
fecha  = today.strftime("%Y-%m-%d")
hora   = time.strftime("%H:%M:%S") 

#Crea tus vista aqui.
def historiales(request,mod):
    ip        = request.META['REMOTE_ADDR']
    equipo    = request.META['HTTP_USER_AGENT']
    a = historial()
    a.idusuario_id  = request.user.id
    a.fecha         = fecha
    a.hora          = hora
    a.equipo        = equipo
    a.ip            = ip 
    a.modulo        = mod[0]
    a.accion        = mod[1]
    a.idaccion      = mod[2]
    a.save()


def permi(request,url):
    idp = request.user.idperfil_id
    mod = permisos.objects.filter(idmodulo__url=url, idperfil_id=idp).values('idmodulo__url','buscar','eliminar','editar','insertar','imprimir','ver')
    return mod

@login_required(login_url='/')
def registro_proyecto(request):
    
    estados =  permi(request, "registro_proyecto")
    listaAlumnos = [{'id':con.id,'nombre':con.nombre} for con in alumno.objects.filter(estadosistema= True)]
    listaDocentes = [{'id':con.id,'nombre':con.nombre} for con in docente.objects.filter(estadosistema= True)]
    listaEstado = [{'id':con.id, 'descripcion':con.descripcion} for con in estado.objects.filter(estadosistema= True)]   
    listaSemestre = [{'id':con.id, 'descripcion':con.descripcion} for con in semestre_academico.objects.filter(estadosistema= True).order_by('-id')]    

    if request.method == 'POST': 
        idd = request.POST["id"]
        if idd == "undefined":
            idSemestre = semestre_academico.objects.latest('id')
            idc = idSemestre.id
            formuP = formproyecto(request.POST,request.FILES )
            formuS = formSolicitud(request.POST,request.FILES )
            if formuP.is_valid() and formuS.is_valid():
                formuP.save()
                formuS.save()
                idp = proyecto.objects.latest('id')
                ids = solicitud.objects.latest('id')
                pp = proyecto.objects.get(pk=idp.id)
                pp.idsolicitud_id = int(ids.id)
                pp.save()
                
                lid = zip(request.POST.getlist('iddocente[]'), request.POST.getlist('modalidaddocente[]'))
                for i,j in lid: 
                    
                    try:
                        iss = proyeDo.objects.latest('id')
                        iss = iss.id
                    except proyeDo.DoesNotExist:
                        iss = 0

                    doc = proyeDo()
                    doc.id                   = iss + 1 
                    doc.idproyectodocente_id = idp.id
                    doc.modalidaddocente_id  = j
                    doc.iddocente_id         = i
                    doc.save()

                lia = zip(request.POST.getlist('idalumno[]'), request.POST.getlist('modalidadalumno[]'))
                for i,j in lia: 
                    try:
                        iss = proyeAl.objects.latest('id')
                        iss = iss.id
                    except proyeAl.DoesNotExist:
                        iss = 0

                    al = proyeAl()
                    al.id                  = iss + 1   
                    al.idproyectoalumno_id = idp.id
                    al.modalidadalumno_id  = j
                    al.idalumno_id         = i
                    al.save()

                liad = zip(request.POST.getlist('idadmistrativo[]'), request.POST.getlist('modalidadadmistrativo[]'))
                for i,j in liad: 
                    try:
                        iss = proyeAd.objects.latest('id')
                        iss = iss.id
                    except proyeAd.DoesNotExist:
                        iss = 0
                    ad = proyeAd()            
                    ad.id                          = iss + 1 
                    ad.idproyectoadministrativo_id = idp.id
                    ad.modalidadadministrativo_id  = j
                    ad.idadministrativo_id         = i
                    ad.save()    

                liex = zip(request.POST.getlist('idexterno[]'), request.POST.getlist('modalidadexterno[]'))
                for i,j in liex: 
                    try:
                        iss = proyeEx.objects.latest('id')
                        iss = iss.id
                    except proyeEx.DoesNotExist:
                        iss = 0

                    ex = proyeEx()
                    ex.id                   = iss + 1 
                    ex.idproyectoexterno_id = idp.id
                    ex.modalidadexterno_id  = j
                    ex.idpexterno_id        = i
                    ex.save()

                historiales(request,["solicitud","registrar",ids.id])
                historiales(request,["proyecto","registrar",idp.id])
            else:
                return render(request,'proyecto/proyecto/form_pro.html',{'formu':formuP})
        else:
            idc = request.POST["id"]
        
        proyectos = proyecto.objects.filter(idsemestre= idc, estado= True).order_by('-id')  
        return render(request,'proyecto/proyecto/ajax_proyecto.html',{'lista':proyectos,'n':'proyectoU','estado':estados})            
    else:
        ids= semestre_academico.objects.latest('id')
        proyectos = proyecto.objects.filter(idsemestre= ids.id, estado= True).order_by('-id')
        formu = formproyecto()
        modulo = {'formu':formu, 'url':'registro_proyecto/','n':'proyectoU','estado':estados,'idalumno':listaAlumnos, 'iddocente':listaDocentes, 'idestado':listaEstado, 'idsemestre_academico':listaSemestre}
        return paginacion(request,proyectos, modulo, 'proyecto/proyecto/proyecto.html' )        

@login_required(login_url='/')
def actualizar_proyecto(request):
    proyectos = proyecto.objects.filter(estado= True).order_by('-id')
    estado =  permi(request, "registro_proyecto")
    if request.method == 'POST': 
        idp = request.POST.get("id","")
        a=get_object_or_404(proyecto,pk=idp)
        form=formproyecto(request.POST, request.FILES, instance=a)
        if form.is_valid():
            form.save()       
            historiales(request,["proyecto","actualizar",idp])
            return render(request,'proyecto/proyecto/ajax_proyecto.html',{'lista':proyectos,'n':'proyectoU','estado':estado}) 
        else:
            return render(request,'proyecto/proyecto/form_proA.html',{'formu':form})             
    else:
        idp = request.GET.get("id","")
        a=get_object_or_404(proyecto,pk=idp)
        form= formproyecto(instance=a)
        return render(request,'proyecto/proyecto/modal.html',{'formu':form,'url':'actualizar_proyecto/','n':'proyectoU','u':'proyectoU','estado':estado})  


@login_required(login_url='/')
def eliminar_proyecto(request):
    proyectos = proyecto.objects.filter(estado= True).order_by('-id')
    estado =  permi(request, "registro_proyecto")
    if request.method == 'GET' and request.is_ajax(): 
        idb = request.GET.get("id","")
        get_object_or_404(proyectos,pk=idb).delete()
        historiales(request,["proyecto","eliminar",idb])
        return render(request,'proyecto/proyecto/ajax_proyecto.html',{'lista':proyectos,'n':'proyectoU','estado':estado})     

@login_required(login_url='/')
def mostrarproyecto(request):
    proyectos = proyecto.objects.filter(pk = request.GET['id'])
    return render(request,'proyecto/proyecto/mostrarproyecto.html',{'lista':proyectos})

@login_required(login_url='/')
def mostrarsolicitud(request):
    solicitudes = proyecto.objects.filter(pk = request.GET['id'])
    return render(request,'proyecto/proyecto/mostrarsolicitud.html',{'lista':solicitudes})  

@login_required(login_url='/')
def editarsolicitud(request):
    if request.method == 'POST': 
        idp = request.POST['id']
        solicitudes = proyecto.objects.filter(idsolicitud_id = idp)
        a=get_object_or_404(solicitud,pk=idp)
        form=formSolicitud(request.POST, request.FILES, instance=a)
        if form.is_valid():
            form.save()       
            historiales(request,["solicitud","actualizar",idp])
            return render(request,'proyecto/proyecto/mostrarsolicitud.html',{'lista':solicitudes}) 

             
        else:
            return render(request,'proyecto/proyecto/form_soli.html',{'formu':form})             
    else:
        idp = request.GET["id"]
        a=get_object_or_404(solicitud,pk=idp)
        formuS = formSolicitud(instance=a)
        return render(request,'proyecto/proyecto/modalS.html',{'nombre':formuS,'url':'editarsolicitud','n':'solicitudU','u':'solicitudU','i':idp}) 

    


@login_required(login_url='/')
def traerparticipantes(request):
    data= []
    doc = []
    alu = []
    adm = []
    otr = []
    mod = []
    par = []
    for con in docente.objects.filter(estadosistema= True).order_by('-id'):
        doc.append({'idd':con.id,'nombres':con.nombre + ' '+ con.apellido, 'codigo':con.codigo})

    for con in alumno.objects.filter(estadosistema= True).order_by('-id'):
        alu.append({'idd':con.id,'nombres':con.nombre + ' '+ con.apellido, 'codigo':con.codigo})

    for con in administrativo.objects.filter(estadosistema= True).order_by('-id'):
        adm.append({'idd':con.id,'nombres':con.nombre + ' '+ con.apellido, 'codigo':con.codigo})

    for con in externo.objects.filter(estadosistema= True).order_by('-id'):
        otr.append({'idd':con.id,'nombres':con.nombre + ' '+ con.apellido, 'codigo':con.codigo})        
    
    for md in modalidad_participante.objects.filter(estadosistema= True):
        mod.append({'idm':md.id, 'modalidad':md.descripcion})

    
    pdl = []    
    if request.GET['id']:
        idp = request.GET['id']
        for pd in proyeDo.objects.filter(idproyectodocente_id= idp).values('iddocente__codigo','modalidaddocente_id','id'):
            pdl.append({'codigo':pd['iddocente__codigo'],'modalidad':pd['modalidaddocente_id'],'idp':pd['id']})
        for al in proyeAl.objects.filter(idproyectoalumno_id= idp).values('idalumno__codigo','modalidadalumno_id','id'):
            pdl.append({'codigo':al['idalumno__codigo'],'modalidad':al['modalidadalumno_id'],'idp':al['id']})
        for ad in proyeAd.objects.filter(idproyectoadministrativo_id= idp).values('idadministrativo__codigo','modalidadadministrativo_id','id'):
            pdl.append({'codigo':ad['idadministrativo__codigo'],'modalidad':ad['modalidadadministrativo_id'],'idp':ad['id']})
        for ex in proyeEx.objects.filter(idproyectoexterno_id= idp).values('idpexterno__codigo','modalidadexterno_id','id'):
            pdl.append({'codigo':ex['idpexterno__codigo'],'modalidad':ex['modalidadexterno_id'],'idp':ex['id']})


    data.append(doc)
    data.append(alu)
    data.append(adm)
    data.append(otr)
    data.append(mod)
    data.append(pdl)

    return JsonResponse(data, safe = False)

@login_required(login_url='/')
def mostrarparticipante(request):
    idp = request.GET['id']
    do = proyeDo.objects.filter(idproyectodocente_id= idp).values('iddocente__codigo','iddocente__nombre','iddocente__apellido','modalidaddocente__descripcion')
    al = proyeAl.objects.filter(idproyectoalumno_id= idp).values('idalumno__codigo','idalumno__nombre','idalumno__apellido','modalidadalumno__descripcion')
    ad = proyeAd.objects.filter(idproyectoadministrativo_id= idp).values('idadministrativo__codigo','idadministrativo__nombre','idadministrativo__apellido','modalidadadministrativo__descripcion')
    ex = proyeEx.objects.filter(idproyectoexterno_id= idp).values('idpexterno__codigo','idpexterno__nombre','idpexterno__apellido','modalidadexterno__descripcion')

    return render(request,'proyecto/proyecto/participantes/mostrarparticipante.html',{'docente':do,'alumno':al,'administrativo':ad,'externo':ex,'id':idp})  

@login_required(login_url='/')
def actualizarparticipante(request):
    idp = int(request.POST['id'])    


    lid = zip(request.POST.getlist('iddocente[]'), request.POST.getlist('modalidaddocente[]'), request.POST.getlist('idmoddocente[]'))
    pp = proyeDo.objects.filter(idproyectodocente_id = idp)
    md = request.POST.getlist('idmoddocente[]')

    for pr in pp:
        if (str(pr.id) in md) == 0 :
            get_object_or_404(proyeDo,pk=pr.id).delete()

    for i,j,k in lid: 
        #print(str(i)+ " , "+ str(j)+ " , " + str(k))
        if k == "undefined":
            try:
                iss = proyeDo.objects.latest('id')
                iss = iss.id
            except proyeDo.DoesNotExist:
                iss = 0

            doc = proyeDo()
            doc.id                   = iss + 1               
            doc.idproyectodocente_id = idp
            doc.modalidaddocente_id  = j
            doc.iddocente_id         = i
            doc.save()
        else:
            doc = proyeDo(pk=k)
            doc.idproyectodocente_id = idp
            doc.modalidaddocente_id = j
            doc.iddocente_id         = i
            doc.save()

    lia = zip(request.POST.getlist('idalumno[]'), request.POST.getlist('modalidadalumno[]'), request.POST.getlist('idmodalumno[]'))
    pp = proyeAl.objects.filter(idproyectoalumno_id = idp)
    md = request.POST.getlist('idmodalumno[]')
    for pr in pp:
        if (str(pr.id) in md) == 0:
            get_object_or_404(proyeAl,pk=pr.id).delete()    
    for i,j,k in lia: 
        if k == "undefined":
            try:
                iss = proyeAl.objects.latest('id')
                iss = iss.id
            except proyeAl.DoesNotExist:
                iss = 0

            al = proyeAl()
            al.id                  = iss + 1             
            al.idproyectoalumno_id = idp
            al.modalidadalumno_id  = j
            al.idalumno_id         = i
            al.save()     
        else:       
            al = proyeAl(pk=k)
            al.idproyectoalumno_id = idp
            al.modalidadalumno_id = j
            al.idalumno_id         = i
            al.save()

    
    liad = zip(request.POST.getlist('idadministrativo[]'), request.POST.getlist('modalidadadministrativo[]'), request.POST.getlist('idmodadministrativo[]'))
    pp = proyeAd.objects.filter(idproyectoadministrativo_id = idp)
    md = request.POST.getlist('idmodadministrativo[]')
    for pr in pp:
        if (str(pr.id) in md) == 0:
            get_object_or_404(proyeAd,pk=pr.id).delete()       
    for i,j,k in liad: 
        if k == "undefined":
            try:
                iss = proyeAd.objects.latest('id')
                iss = iss.id
            except proyeAd.DoesNotExist:
                iss = 0
            ad = proyeAd()            
            ad.id                          = iss + 1 
            ad.idproyectoadministrativo_id = idp
            ad.modalidadadministrativo_id  = j
            ad.idadministrativo_id         = i
            ad.save()    
        else:
            ad = proyeAd(pk=k)
            ad.idproyectoadministrativo_id = idp
            ad.modalidadadministrativo_id  = j
            ad.idadministrativo_id         = i
            ad.save()               

    liex = zip(request.POST.getlist('idexterno[]'), request.POST.getlist('modalidadexterno[]'), request.POST.getlist('idmodexterno[]'))
    pp = proyeEx.objects.filter(idproyectoexterno_id = idp)
    md = request.POST.getlist('idmodexterno[]')
    for pr in pp:
        if (str(pr.id) in md) == 0:
            get_object_or_404(proyeEx,pk=pr.id).delete()         
    for i,j,k in liex: 
        if k == "undefined":
            try:
                iss = proyeEx.objects.latest('id')
                iss = iss.id
            except proyeEx.DoesNotExist:
                iss = 0

            ex = proyeEx()
            ex.id                   = iss + 1             
            ex.idproyectoexterno_id = idp
            ex.modalidadexterno_id  = j
            ex.idpexterno_id        = i
            ex.save()
        else:
            ex = proyeEx(pk=k)
            ex.idproyectoexterno_id = idp
            ex.modalidadexterno_id = j
            ex.idpexterno_id         = i
            ex.save()


    do = proyeDo.objects.filter(idproyectodocente_id= idp).values('iddocente__codigo','iddocente__nombre','iddocente__apellido','modalidaddocente__descripcion')
    al = proyeAl.objects.filter(idproyectoalumno_id= idp).values('idalumno__codigo','idalumno__nombre','idalumno__apellido','modalidadalumno__descripcion')
    ad = proyeAd.objects.filter(idproyectoadministrativo_id= idp).values('idadministrativo__codigo','idadministrativo__nombre','idadministrativo__apellido','modalidadadministrativo__descripcion')
    ex = proyeEx.objects.filter(idproyectoexterno_id= idp).values('idpexterno__codigo','idpexterno__nombre','idpexterno__apellido','modalidadexterno__descripcion')

    return render(request,'proyecto/proyecto/participantes/mostrarparticipante.html',{'docente':do,'alumno':al,'administrativo':ad,'externo':ex,'id':idp})  


@login_required(login_url='/')
def Adocente(request):
    if request.method == 'POST': 
        formH = formDocente(request.POST)
        if formH.is_valid():
            formH.save()
            idi = docente.objects.latest('id')  
            historiales(request,["docente","registrar",idi.id])
            return HttpResponse("ok")            
        else:
            return render(request,'proyecto/proyecto/participantes/form.html',{'formu':formH})                

    else:
        formH = formDocente()
        return render(request,'proyecto/proyecto/participantes/modal.html',{'nombre':formH,'url':'adocente','n':'AdocenteU','t':'docente', 'u':'AdocenteU'} )        

@login_required(login_url='/')
def Aalumno(request):
    if request.method == 'POST': 
        formH = formAlumno(request.POST)
        if formH.is_valid():
            formH.save()
            idi = alumno.objects.latest('id')  
            historiales(request,["alumno","registrar",idi.id])
            return HttpResponse("ok")            
        else:
            return render(request,'proyecto/proyecto/participantes/form.html',{'formu':formH})                

    else:
        formH = formAlumno()
        return render(request,'proyecto/proyecto/participantes/modal.html',{'nombre':formH,'url':'aalumno','n':'AalumnoU', 'u':'AalumnoU','t':'alumno',} )        

@login_required(login_url='/')
def Aadministrativo(request):
    if request.method == 'POST': 
        formH = formAdministrativo(request.POST)
        if formH.is_valid():
            formH.save()
            idi = administrativo.objects.latest('id')  
            historiales(request,["administrativo","registrar",idi.id])
            return HttpResponse("ok")            
        else:
            return render(request,'proyecto/proyecto/participantes/form.html',{'formu':formH})                

    else:
        formH = formAdministrativo()
        return render(request,'proyecto/proyecto/participantes/modal.html',{'nombre':formH,'url':'aadministrativo','n':'AadministrativoU', 'u':'AadministrativoU','t':'administrativo',} )        

@login_required(login_url='/')
def Aexterno(request):
    if request.method == 'POST': 
        formH = formExterno(request.POST)
        if formH.is_valid():
            formH.save()
            idi = externo.objects.latest('id')  
            historiales(request,["externo","registrar",idi.id])
            return HttpResponse("ok")            
        else:
            return render(request,'proyecto/proyecto/participantes/form_ex.html',{'formu':formH})                

    else:
        formH = formExterno()
        return render(request,'proyecto/proyecto/participantes/modal.html',{'nombre':formH,'url':'aexterno','n':'AexternoU', 'u':'AexternoU','t':'externo',} )        


def busq_ajax_pro(request):
    dat = request.GET.get('datos')
    estado =  permi(request, "registro_proyecto")
    e = proyecto.objects.filter( Q(documento__contains=dat), Q(asunto__contains=dat), Q(idestado__descripcion__contains=dat))[:10]
    modulo = {'lista':e,'n':'proyectoU','estado':estado}
    return render(request,'proyecto/proyecto/ajax_proyecto.html', modulo)

@login_required(login_url='/')
def cambiarestado(request):
    pp = proyecto.objects.get(pk=int(request.GET["id"]))
    pp.idestado_id  = int(request.GET['estado'])
    pp.save()
    return HttpResponse("ok")

@login_required(login_url='/')
def registro_informe(request):
    listaOficinas = [{'id':con.id,'oficina':con.oficina} for con in oficina.objects.filter(estadosistema= True)]
    estados =  permi(request, "registro_proyecto")
    if request.method == 'POST': 
        idd = request.POST["id"]
        formH = formInforme(request.POST,request.FILES )
        if formH.is_valid():
            formH.save()
            idi = informe.objects.latest('id')  

            pp = proyecto.objects.get(pk=idd)
            pp.idinforme_id = int(idi.id)
            pp.idestado_id  = 2
            pp.save()     
            idSemestre = semestre_academico.objects.latest('id')
            idc = idSemestre.id
            historiales(request,["informe","registrar",idi.id])
            proyectos = proyecto.objects.filter(idsemestre= idc, estado= True).order_by('-id')  
            return render(request,'proyecto/proyecto/ajax_proyecto.html',{'lista':proyectos,'n':'proyectoU','estado':estados})             
        else:
            return render(request,'proyecto/proyecto/informe/form_in.html',{'formu':formH})                

    else:
        idi = request.GET["id"]
        informes = informe.objects.filter(estado= True).order_by('id')            
        formH = formInforme()
        return render(request,'proyecto/proyecto/informe/modalinforme.html',{'f':'F','nombre':formH,'i':idi,'url':'registro_informe/','n':'registro_informeU', 'u':'proyectoU'} )        


@login_required(login_url='/')
def mostrarinforme(request):
    informes = proyecto.objects.filter(pk = request.GET['id'])
    return render(request,'proyecto/proyecto/informe/mostrarinforme.html',{'lista':informes})  


@login_required(login_url='/')
def editarinforme(request):
    if request.method == 'POST': 
        idp = request.POST['id']
        informes = proyecto.objects.filter(idinforme_id = idp)
        a=get_object_or_404(informe,pk=idp)
        form=formInforme(request.POST, request.FILES, instance=a)
        if form.is_valid():
            form.save()       
            historiales(request,["informe","actualizar",idp])
            return render(request,'proyecto/proyecto/informe/mostrarinforme.html',{'lista':informes}) 
        else:
            return render(request,'proyecto/proyecto/informe/form_in.html',{'formu':form})             
    else:
        idp = request.GET["id"]
        a=get_object_or_404(informe,pk=idp)
        formuI = formInforme(instance=a)
        return render(request,'proyecto/proyecto/informe/modalinforme.html',{'nombre':formuI,'f':'SO','url':'editarinforme','n':'informeU','u':'informeU','i':idp}) 

@login_required(login_url='/')
def registro_resolucion(request):
    listaOficinas = [{'id':con.id,'oficina':con.oficina} for con in oficina.objects.filter(estadosistema= True)]
    estados =  permi(request, "registro_proyecto")
    if request.method == 'POST': 
        idd = request.POST["id"]
        formH = formResolucion(request.POST,request.FILES )
        if formH.is_valid():
            formH.save()
            idi = resolucion.objects.latest('id')  

            pp = proyecto.objects.get(pk=idd)
            pp.idresolucion_id = int(idi.id)
            pp.idestado_id     = 3
            pp.save()     
            idSemestre = semestre_academico.objects.latest('id')
            idc = idSemestre.id
            historiales(request,["resolucion","registrar",idi.id])
            proyectos = proyecto.objects.filter(idsemestre= idc, estado= True).order_by('-id')  
            return render(request,'proyecto/proyecto/ajax_proyecto.html',{'lista':proyectos,'n':'proyectoU','estado':estados})             
        else:
            return render(request,'proyecto/proyecto/resolucion/form_re.html',{'formu':formH})                

    else:
        idi = request.GET["id"]
        formH = formResolucion()
        return render(request,'proyecto/proyecto/resolucion/modalresolucion.html',{'f':'F','nombre':formH,'i':idi,'url':'registro_resolucion/','n':'registro_resolucionU', 'u':'proyectoU'} )        


@login_required(login_url='/')
def mostrarresolucion(request):
    resolucions = proyecto.objects.filter(pk = request.GET['id'])
    return render(request,'proyecto/proyecto/resolucion/mostrarresolucion.html',{'lista':resolucions})  


@login_required(login_url='/')
def editarresolucion(request):
    if request.method == 'POST': 
        idp = request.POST['id']
        resolucions = proyecto.objects.filter(idresolucion_id = idp)
        a=get_object_or_404(resolucion,pk=idp)
        form=formResolucion(request.POST, request.FILES, instance=a)
        if form.is_valid():
            form.save()       
            historiales(request,["resolucion","actualizar",idp])
            return render(request,'proyecto/proyecto/resolucion/mostrarresolucion.html',{'lista':resolucions}) 
        else:
            return render(request,'proyecto/proyecto/resolucion/form_re.html',{'formu':form})             
    else:
        idp = request.GET["id"]
        a=get_object_or_404(resolucion,pk=idp)
        formuI = formResolucion(instance=a)
        return render(request,'proyecto/proyecto/resolucion/modalresolucion.html',{'nombre':formuI,'f':'SO','url':'editarresolucion','n':'resolucionU','u':'resolucionU','i':idp}) 

        