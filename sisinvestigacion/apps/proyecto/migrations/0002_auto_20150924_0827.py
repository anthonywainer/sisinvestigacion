# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('proyecto', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='proyead',
            old_name='modalidadalumno',
            new_name='modalidadadministrativo',
        ),
        migrations.RenameField(
            model_name='proyeex',
            old_name='modalidadalumno',
            new_name='modalidadexterno',
        ),
    ]
