# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('matenimiento', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='informe',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('numeroinforme', models.CharField(max_length=100)),
                ('asuntoinforme', models.CharField(max_length=300)),
                ('proveidoinforme', models.CharField(max_length=300)),
                ('fechadocumento', models.DateField()),
                ('fechaenvio', models.DateField()),
                ('archivoinforme', models.FileField(upload_to='Informe/%Y/%m/%d')),
                ('estado', models.BooleanField(default=True)),
                ('idoficina', models.ForeignKey(null=True, to='matenimiento.oficina')),
            ],
        ),
        migrations.CreateModel(
            name='proyeAd',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('idadministrativo', models.ForeignKey(to='matenimiento.administrativo')),
            ],
        ),
        migrations.CreateModel(
            name='proyeAl',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('idalumno', models.ForeignKey(to='matenimiento.alumno')),
            ],
        ),
        migrations.CreateModel(
            name='proyecto',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('documento', models.CharField(max_length=200)),
                ('fecharecepcion', models.DateField()),
                ('fechaemision', models.DateField(null=True)),
                ('fechainicio', models.DateField()),
                ('fechatermino', models.DateField()),
                ('asunto', models.TextField(max_length=500)),
                ('proveido', models.CharField(max_length=200, null=True)),
                ('archivo', models.FileField(upload_to='Proyecto/%Y/%m/%d')),
                ('observacion', models.TextField(max_length=500, null=True)),
                ('estado', models.BooleanField(default=True)),
                ('idadministrativo', models.ManyToManyField(through='proyecto.proyeAd', to='matenimiento.administrativo')),
                ('idalumno', models.ManyToManyField(through='proyecto.proyeAl', to='matenimiento.alumno')),
            ],
        ),
        migrations.CreateModel(
            name='proyeDo',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('iddocente', models.ForeignKey(to='matenimiento.docente')),
                ('idproyectodocente', models.ForeignKey(to='proyecto.proyecto')),
                ('modalidaddocente', models.ForeignKey(to='matenimiento.modalidad_participante')),
            ],
        ),
        migrations.CreateModel(
            name='proyeEx',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('idpexterno', models.ForeignKey(to='matenimiento.externo')),
                ('idproyectoexterno', models.ForeignKey(to='proyecto.proyecto')),
                ('modalidadalumno', models.ForeignKey(to='matenimiento.modalidad_participante')),
            ],
        ),
        migrations.CreateModel(
            name='resolucion',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('numeroresolucion', models.CharField(max_length=20)),
                ('asuntoresolucion', models.CharField(max_length=300)),
                ('proveidoresolucion', models.CharField(max_length=200)),
                ('fechadocumento', models.DateField()),
                ('fechaenvio', models.DateField()),
                ('archivoresolucion', models.FileField(upload_to='Resolucion/%Y/%m/%d')),
                ('estado', models.BooleanField(default=True)),
                ('idoficina', models.ForeignKey(null=True, to='matenimiento.oficina')),
            ],
        ),
        migrations.CreateModel(
            name='solicitud',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombresolicitud', models.CharField(max_length=100)),
                ('asuntosolicitud', models.CharField(max_length=300)),
                ('proveidosolicitud', models.CharField(max_length=200)),
                ('fechadocumento', models.DateField()),
                ('archivosolicitud', models.FileField(upload_to='Solicitud/%Y/%m/%d')),
                ('estado', models.BooleanField(default=True)),
            ],
        ),
        migrations.AddField(
            model_name='proyecto',
            name='iddocente',
            field=models.ManyToManyField(through='proyecto.proyeDo', to='matenimiento.docente'),
        ),
        migrations.AddField(
            model_name='proyecto',
            name='idestado',
            field=models.ForeignKey(to='matenimiento.estado'),
        ),
        migrations.AddField(
            model_name='proyecto',
            name='idexterno',
            field=models.ManyToManyField(through='proyecto.proyeEx', to='matenimiento.externo'),
        ),
        migrations.AddField(
            model_name='proyecto',
            name='idinforme',
            field=models.ForeignKey(null=True, to='proyecto.informe'),
        ),
        migrations.AddField(
            model_name='proyecto',
            name='idresolucion',
            field=models.ForeignKey(null=True, to='proyecto.resolucion'),
        ),
        migrations.AddField(
            model_name='proyecto',
            name='idsemestre',
            field=models.ForeignKey(to='matenimiento.semestre_academico'),
        ),
        migrations.AddField(
            model_name='proyecto',
            name='idsolicitud',
            field=models.ForeignKey(null=True, to='proyecto.solicitud'),
        ),
        migrations.AddField(
            model_name='proyeal',
            name='idproyectoalumno',
            field=models.ForeignKey(to='proyecto.proyecto'),
        ),
        migrations.AddField(
            model_name='proyeal',
            name='modalidadalumno',
            field=models.ForeignKey(to='matenimiento.modalidad_participante'),
        ),
        migrations.AddField(
            model_name='proyead',
            name='idproyectoadministrativo',
            field=models.ForeignKey(to='proyecto.proyecto'),
        ),
        migrations.AddField(
            model_name='proyead',
            name='modalidadalumno',
            field=models.ForeignKey(to='matenimiento.modalidad_participante'),
        ),
    ]
