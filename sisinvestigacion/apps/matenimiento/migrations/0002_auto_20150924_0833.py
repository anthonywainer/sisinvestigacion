# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('matenimiento', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='docente',
            name='correo',
            field=models.CharField(null=True, max_length=100),
        ),
    ]
