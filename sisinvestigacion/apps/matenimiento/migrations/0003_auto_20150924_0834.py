# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('matenimiento', '0002_auto_20150924_0833'),
    ]

    operations = [
        migrations.AlterField(
            model_name='docente',
            name='dni',
            field=models.CharField(null=True, max_length=8),
        ),
    ]
