# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='administrativo',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(max_length=100)),
                ('apellido', models.CharField(max_length=150)),
                ('dni', models.CharField(max_length=8, null=True)),
                ('codigo', models.CharField(max_length=20)),
                ('celular', models.CharField(max_length=10, null=True)),
                ('ocupacion', models.CharField(max_length=100, null=True)),
                ('estadosistema', models.BooleanField(default=True)),
            ],
        ),
        migrations.CreateModel(
            name='alumno',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(max_length=100)),
                ('apellido', models.CharField(max_length=150)),
                ('codigo', models.CharField(unique=True, max_length=8)),
                ('estadosistema', models.BooleanField(default=True)),
            ],
        ),
        migrations.CreateModel(
            name='docente',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(max_length=100)),
                ('apellido', models.CharField(max_length=100)),
                ('codigo', models.CharField(max_length=20)),
                ('dni', models.CharField(unique=True, max_length=8, null=True)),
                ('correo', models.CharField(unique=True, max_length=100, null=True)),
                ('estadosistema', models.BooleanField(default=True)),
            ],
        ),
        migrations.CreateModel(
            name='estado',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('descripcion', models.CharField(max_length=100)),
                ('estadosistema', models.BooleanField(default=True)),
            ],
        ),
        migrations.CreateModel(
            name='externo',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(max_length=100)),
                ('apellido', models.CharField(max_length=150)),
                ('dni', models.CharField(max_length=8, null=True)),
                ('codigo', models.CharField(max_length=20)),
                ('celular', models.CharField(max_length=10, null=True)),
                ('formacionacademica', models.CharField(max_length=200, null=True)),
                ('ocupacion', models.CharField(max_length=100, null=True)),
                ('centrolabores', models.CharField(max_length=100, null=True)),
                ('estadosistema', models.BooleanField(default=True)),
            ],
        ),
        migrations.CreateModel(
            name='modalidad_participante',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('descripcion', models.CharField(max_length=100)),
                ('estadosistema', models.BooleanField(default=True)),
            ],
        ),
        migrations.CreateModel(
            name='oficina',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('oficina', models.CharField(max_length=100)),
                ('estadosistema', models.BooleanField(default=True)),
            ],
        ),
        migrations.CreateModel(
            name='semestre_academico',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('descripcion', models.CharField(max_length=8)),
                ('fechainicio', models.DateField()),
                ('fechatermino', models.DateField()),
                ('estadosistema', models.BooleanField(default=True)),
            ],
        ),
        migrations.AddField(
            model_name='administrativo',
            name='idoficina',
            field=models.ForeignKey(null=True, to='matenimiento.oficina'),
        ),
    ]
