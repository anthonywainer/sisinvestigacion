from django.db import models

class docente(models.Model):
    nombre        = models.CharField(max_length=100)
    apellido      = models.CharField(max_length=100)
    codigo        = models.CharField(max_length=20)
    dni           = models.CharField(max_length=8, null=True)
    correo        = models.CharField(max_length=100,null=True)
    estadosistema = models.BooleanField(default=True)

class alumno(models.Model):
    nombre = models.CharField(max_length=100)
    apellido = models.CharField(max_length=150)
    codigo = models.CharField(max_length=8, unique=True)
    estadosistema = models.BooleanField(default=True)

class oficina(models.Model):
    oficina = models.CharField(max_length=100)
    estadosistema = models.BooleanField(default=True)

class modalidad_participante(models.Model):
    descripcion = models.CharField(max_length=100)
    estadosistema = models.BooleanField(default=True)


class estado(models.Model):
    descripcion = models.CharField(max_length=100)
    estadosistema = models.BooleanField(default=True)

class semestre_academico(models.Model):
    descripcion = models.CharField(max_length=8)
    fechainicio = models.DateField()
    fechatermino = models.DateField()
    estadosistema = models.BooleanField(default=True)

class externo(models.Model):
    nombre = models.CharField(max_length=100)
    apellido = models.CharField(max_length=150)
    dni = models.CharField(max_length=8, null=True)
    codigo = models.CharField(max_length=20)
    celular = models.CharField(max_length=10,null=True)
    formacionacademica = models.CharField(max_length=200,null=True)
    ocupacion = models.CharField(max_length=100, null=True)
    centrolabores = models.CharField(max_length=100, null=True)
    estadosistema = models.BooleanField(default=True)

class administrativo(models.Model):
    nombre = models.CharField(max_length=100)
    apellido = models.CharField(max_length=150)
    dni = models.CharField(max_length=8, null=True)
    codigo = models.CharField(max_length=20)
    celular = models.CharField(max_length=10,null=True)
    ocupacion = models.CharField(max_length=100, null=True)
    idoficina = models.ForeignKey(oficina, null=True)
    estadosistema = models.BooleanField(default=True)

