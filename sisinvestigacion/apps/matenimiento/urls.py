from django.conf.urls import patterns, url, include
from .views import *

urlpatterns = [

	#oficina
    url(r'^registro_oficina/$', registro_oficina),
    url(r'^actualizar_oficina/$', actualizar_oficina),
    url(r'^eliminar_oficina/$', eliminar_oficina),
    #semestre_academico
    url(r'^registro_semestre_academico/$', registro_semestre_academico),
    url(r'^actualizar_semestre_academico/$', actualizar_semestre_academico),
    url(r'^eliminar_semestre_academico/$', eliminar_semestre_academico),

    #alumno
    url(r'^registro_alumno/$', registro_alumno),
    url(r'^actualizar_alumno/$', actualizar_alumno),
    url(r'^eliminar_alumno/$', eliminar_alumno),

    #docente
    url(r'^registro_docente/$', registro_docente),
    url(r'^actualizar_docente/$', actualizar_docente),
    url(r'^eliminar_docente/$', eliminar_docente),

    #modalidad_participante
    url(r'^registro_modalidad_participante/$', registro_modalidad_participante),
    url(r'^actualizar_modalidad_participante/$', actualizar_modalidad_participante),
    url(r'^eliminar_modalidad_participante/$', eliminar_modalidad_participante),

    #estado
    url(r'^registro_estado/$', registro_estado),
    url(r'^actualizar_estado/$', actualizar_estado),
    url(r'^eliminar_estado/$', eliminar_estado),

    #externo
    url(r'^registro_externo/$', registro_externo),
    url(r'^actualizar_externo/$', actualizar_externo),
    url(r'^eliminar_externo/$', eliminar_externo),

    #administrativo
    url(r'^registro_administrativo/$', registro_administrativo),
    url(r'^actualizar_administrativo/$', actualizar_administrativo),
    url(r'^eliminar_administrativo/$', eliminar_administrativo),
]