from django import forms
from django.forms import ModelForm
from .models import oficina, semestre_academico, alumno, docente, modalidad_participante, estado, externo, administrativo

class formOficina(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(formOficina, self).__init__(*args, **kwargs)
        self.fields['oficina'].widget = forms.TextInput(attrs={'class':'form-control input-sm', 'required':'', 'placeholder':'ingrese oficina'})

    class Meta:
        model = oficina
        exclude = ['estadosistema']

class formSemestre_academico(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(formSemestre_academico, self).__init__(*args, **kwargs)
        self.fields['descripcion'].widget = forms.TextInput(attrs={'class':'form-control input-sm', 'required':'', 'placeholder':'ingrese semestre academico'})
        self.fields['fechainicio'].widget = forms.DateInput(attrs={'class':'form-control input-sm', 'type':'date' ,'required':''})
        self.fields['fechatermino'].widget = forms.DateInput(attrs={'class':'form-control input-sm', 'type':'date' ,'required':''})

    class Meta:
        model = semestre_academico
        exclude = ['estadosistema']

class formAlumno(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(formAlumno, self).__init__(*args, **kwargs)
        self.fields['nombre'].widget = forms.TextInput(attrs={'class':'form-control input-sm', 'required':'', 'placeholder':'ingrese nombre del alumno'})
        self.fields['apellido'].widget = forms.TextInput(attrs={'class':'form-control input-sm' ,'required':'','placeholder':'ingrese apellido del alumno'})
        self.fields['codigo'].widget = forms.TextInput(attrs={'class':'form-control input-sm' ,'required':'','placeholder':'ingrese codigo del alumno'})

    class Meta:
        model = alumno
        exclude = ['estadosistema']

class formDocente(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(formDocente, self).__init__(*args, **kwargs)
        self.fields['nombre'].widget = forms.TextInput(attrs={'class':'form-control input-sm', 'required':'', 'placeholder':'ingrese nombre del docente'})
        self.fields['apellido'].widget = forms.TextInput(attrs={'class':'form-control input-sm' ,'required':'','placeholder':'ingrese apellido del docente'})
        self.fields['dni'].widget = forms.TextInput(attrs={'class':'form-control input-sm' ,'required':'','placeholder':'ingrese DNI del docente'})
        self.fields['correo'].widget = forms.TextInput(attrs={'class':'form-control input-sm' ,'required':'','placeholder':'ingrese email del docente'})

    class Meta:
        model = docente
        exclude = ['estadosistema']

class formModalidad_participante(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(formModalidad_participante, self).__init__(*args, **kwargs)
        self.fields['descripcion'].widget = forms.TextInput(attrs={'class':'form-control input-sm', 'required':'', 'placeholder':'ingrese modalidad del participante'})

    class Meta:
        model = modalidad_participante
        exclude = ['estadosistema']


class formEstado_proyecto(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(formEstado_proyecto, self).__init__(*args, **kwargs)
        self.fields['descripcion'].widget = forms.TextInput(attrs={'class':'form-control input-sm', 'required':'', 'placeholder':'ingrese estado del proyecto'})

    class Meta:
        model = estado
        exclude = ['estadosistema']

class formExterno(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(formExterno, self).__init__(*args, **kwargs)
        self.fields['nombre'].widget = forms.TextInput(attrs={'class':'form-control input-sm', 'required':'', 'placeholder':'ingrese nombre del participante'})
        self.fields['apellido'].widget = forms.TextInput(attrs={'class':'form-control input-sm' ,'required':'','placeholder':'ingrese apellido del participante'})
        self.fields['dni'].widget = forms.TextInput(attrs={'class':'form-control input-sm' ,'required':'','placeholder':'ingrese DNI del participante'})
        self.fields['codigo'].widget = forms.TextInput(attrs={'class':'form-control input-sm' ,'required':'','placeholder':'ingrese codigo del participante'})
        self.fields['celular'].widget = forms.TextInput(attrs={'class':'form-control input-sm' ,'required':'','placeholder':'ingrese email del participante'})
        self.fields['formacionacademica'].widget = forms.TextInput(attrs={'class':'form-control input-sm' ,'required':'','placeholder':'ingrese email del participante'})
        self.fields['ocupacion'].widget = forms.TextInput(attrs={'class':'form-control input-sm' ,'required':'','placeholder':'ingrese email del participante'})
        self.fields['centrolabores'].widget = forms.TextInput(attrs={'class':'form-control input-sm' ,'required':'','placeholder':'ingrese email del participante'})

    class Meta:
        model = externo
        exclude = ['estadosistema']

listaOficinas = [(con.id, con.oficina) for con in oficina.objects.filter(estadosistema= True)]
class formAdministrativo(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(formAdministrativo, self).__init__(*args, **kwargs)
        self.fields['nombre'].widget = forms.TextInput(attrs={'class':'form-control input-sm', 'required':'', 'placeholder':'ingrese nombre del administrativo'})
        self.fields['apellido'].widget = forms.TextInput(attrs={'class':'form-control input-sm' ,'required':'','placeholder':'ingrese apellido del administrativo'})
        self.fields['dni'].widget = forms.TextInput(attrs={'class':'form-control input-sm' ,'required':'','placeholder':'ingrese DNI del administrativo'})
        self.fields['codigo'].widget = forms.TextInput(attrs={'class':'form-control input-sm' ,'required':'','placeholder':'ingrese codigo del administrativo'})
        self.fields['celular'].widget = forms.TextInput(attrs={'class':'form-control input-sm' ,'required':'','placeholder':'ingrese email del administrativo'})
        self.fields['ocupacion'].widget = forms.TextInput(attrs={'class':'form-control input-sm' ,'required':'','placeholder':'ingrese ocupacion del administrativo'})
        self.fields['idoficina'].widget = forms.Select( choices=listaOficinas,attrs={'class':'form-control chosen-select'})

    class Meta:
        model = administrativo
        exclude = ['estadosistema']