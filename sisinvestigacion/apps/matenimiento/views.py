from django.shortcuts import render,get_object_or_404,redirect
from django.http import HttpResponse
from django.contrib.auth.decorators import login_required
from apps.seguridad.models import permisos
from .forms import  formOficina, formSemestre_academico, formAlumno, formDocente, formModalidad_participante, formEstado_proyecto, formExterno, formAdministrativo
from .models import oficina, semestre_academico, alumno, docente, modalidad_participante, estado, externo, administrativo

# Crea tus vista aqui.
def permi(request,url):
    idp = request.user.idperfil_id
    mod = permisos.objects.filter(idmodulo__url=url, idperfil_id=idp).values('idmodulo__url','buscar','eliminar','editar','insertar','imprimir','ver')
    return mod

@login_required(login_url='/')
def registro_oficina(request):
    oficinas = oficina.objects.filter(estadosistema= True).order_by('id')
    estado =  permi(request, "registro_oficina")
    if request.method == 'POST' and request.is_ajax(): 
        formu = formOficina(request.POST)
        if formu.is_valid():
            formu.save()
            return render(request,'mantenimiento/oficina/ajax_oficina.html',{'oficina':oficinas,'n':'oficinaU','estado':estado})            
        else:
            return render(request,'mantenimiento/oficina/form_of.html',{'formu':formu})            
    else:
        formu = formOficina()
        return render(request,'mantenimiento/oficina/oficina.html',{'formu':formu,'oficina':oficinas, 'url':'registro_oficina/','n':'oficinaU','estado':estado})

@login_required(login_url='/')
def eliminar_oficina(request):
    oficinas = oficina.objects.all().order_by('id')
    estado =  permi(request, "registro_oficina")
    if request.method == 'GET' and request.is_ajax():
        idb = request.GET.get("id","")
        a= oficina.objects.get(pk=idb)
        a.estadosistema = False
        a.save()
        #get_object_or_404(oficina,pk=idb).delete()
        return render(request,'mantenimiento/oficina/ajax_oficina.html',{'oficina':oficinas,'n':'oficinaU','estado':estado})     

@login_required(login_url='/')
def actualizar_oficina(request):
    oficinas = oficina.objects.all().order_by('id')
    estado =  permi(request, "registro_oficina")
    if request.method == 'POST' and request.is_ajax():
        idp = request.POST.get("id","")
        a=get_object_or_404(oficina,pk=idp)
        form=formOficina(request.POST, instance=a)
        if form.is_valid():
            form.save()
            return render(request,'mantenimiento/oficina/ajax_oficina.html',{'oficina':oficinas,'n':'oficinaU','estado':estado}) 
        else:
            return render(request,'mantenimiento/oficina/form_of.html',{'formu':form})             
    else:
        idp = request.GET.get("id","")
        a=get_object_or_404(oficina,pk=idp)
        form= formOficina(instance=a)
        return render(request,'seguridad/modal.html',{'nombre':form,'url':'actualizar_oficina/','n':'oficinaU','u':'oficinaU','estado':estado}) 


@login_required(login_url='/')
def registro_semestre_academico(request):
    semestre_academicos = semestre_academico.objects.filter(estadosistema= True).order_by('id')
    estado =  permi(request, "registro_semestre_academico")

    if request.method == 'POST' and request.is_ajax(): 
        formu = formSemestre_academico(request.POST)
        if formu.is_valid():
            formu.save()
            return render(request,'mantenimiento/semestre_academico/ajax_semestre_academico.html',{'semestre_academico':semestre_academicos,'n':'semestre_academicoU','estado':estado})
        else:
            return render(request,'mantenimiento/semestre_academico/form_semestre_academico.html',{'formu':formu})
    else:
        formu = formSemestre_academico()
        return render(request,'mantenimiento/semestre_academico/semestre_academico.html',{'formu':formu,'semestre_academico':semestre_academicos, 'url':'registro_semestre_academico/','n':'semestre_academicoU','estado':estado})

@login_required(login_url='/')
def eliminar_semestre_academico(request):
    semestre_academicos = semestre_academico.objects.all().order_by('id')
    estado =  permi(request, "registro_semestre_academico")
    if request.method == 'GET' and request.is_ajax():
        idb = request.GET.get("id","")
        a= semestre_academico.objects.get(pk=idb)
        a.estadosistema = False
        a.save()
        #get_object_or_404(semestre_academico,pk=idb).delete()
        return render(request,'mantenimiento/semestre_academico/ajax_semestre_academico.html',{'semestre_academico':semestre_academicos,'n':'semestre_academicoU','estado':estado})     

@login_required(login_url='/')
def actualizar_semestre_academico(request):
    semestre_academicos = semestre_academico.objects.all().order_by('id')
    estado =  permi(request, "registro_semestre_academico")
    if request.method == 'POST' and request.is_ajax():
        idp = request.POST.get("id","")
        a=get_object_or_404(semestre_academico,pk=idp)
        form=formSemestre_academico(request.POST, instance=a)
        if form.is_valid():
            form.save()
            return render(request,'mantenimiento/semestre_academico/ajax_semestre_academico.html',{'semestre_academico':semestre_academicos,'n':'semestre_academicoU','estado':estado}) 
        else:
            return render(request,'mantenimiento/semestre_academico/form_semestre_academico.html',{'formu':form})             
    else:
        idp = request.GET.get("id","")
        a=get_object_or_404(semestre_academico,pk=idp)
        form= formSemestre_academico(instance=a)
        return render(request,'seguridad/modal.html',{'nombre':form,'url':'actualizar_semestre_academico/','n':'semestre_academicoU','u':'semestre_academicoU','estado':estado}) 


@login_required(login_url='/')
def registro_alumno(request):
    alumnos = alumno.objects.filter(estadosistema= True).order_by('id')
    estado =  permi(request, "registro_alumno")

    if request.method == 'POST' and request.is_ajax(): 
        formu = formAlumno(request.POST)
        if formu.is_valid():
            formu.save()
            return render(request,'mantenimiento/alumno/ajax_alumno.html',{'alumno':alumnos,'n':'alumnoU','estado':estado})
        else:
            return render(request,'mantenimiento/alumno/form_alumno.html',{'formu':formu})
    else:
        formu = formAlumno()
        return render(request,'mantenimiento/alumno/alumno.html',{'formu':formu,'alumno':alumnos, 'url':'registro_alumno/','n':'alumnoU','estado':estado})

@login_required(login_url='/')
def eliminar_alumno(request):
    alumnos = alumno.objects.all().order_by('id')
    estado =  permi(request, "registro_alumno")
    if request.method == 'GET' and request.is_ajax():
        idb = request.GET.get("id","")
        a= alumno.objects.get(pk=idb)
        a.estadosistema = False
        a.save()
        #get_object_or_404(alumno,pk=idb).delete()
        return render(request,'mantenimiento/alumno/ajax_alumno.html',{'alumno':alumnos,'n':'alumnoU','estado':estado})     

@login_required(login_url='/')
def actualizar_alumno(request):
    alumnos = alumno.objects.all().order_by('id')
    estado =  permi(request, "registro_alumno")
    if request.method == 'POST' and request.is_ajax():
        idp = request.POST.get("id","")
        a=get_object_or_404(alumno,pk=idp)
        form=formAlumno(request.POST, instance=a)
        if form.is_valid():
            form.save()
            return render(request,'mantenimiento/alumno/ajax_alumno.html',{'alumno':alumnos,'n':'alumnoU','estado':estado}) 
        else:
            return render(request,'mantenimiento/alumno/form_alumno.html',{'formu':form})             
    else:
        idp = request.GET.get("id","")
        a=get_object_or_404(alumno,pk=idp)
        form= formAlumno(instance=a)
        return render(request,'seguridad/modal.html',{'nombre':form,'url':'actualizar_alumno/','n':'alumnoU','u':'alumnoU','estado':estado}) 

@login_required(login_url='/')
def registro_docente(request):
    docentes = docente.objects.filter(estadosistema= True).order_by('id')
    estado =  permi(request, "registro_docente")

    if request.method == 'POST' and request.is_ajax(): 
        formu = formDocente(request.POST)
        if formu.is_valid():
            formu.save()
            return render(request,'mantenimiento/docente/ajax_docente.html',{'docente':docentes,'n':'docenteU','estado':estado})
        else:
            return render(request,'mantenimiento/docente/form_docente.html',{'formu':formu})
    else:
        formu = formDocente()
        return render(request,'mantenimiento/docente/docente.html',{'formu':formu,'docente':docentes, 'url':'registro_docente/','n':'docenteU','estado':estado})

@login_required(login_url='/')
def eliminar_docente(request):
    docentes = docente.objects.all().order_by('id')
    estado =  permi(request, "registro_docente")
    if request.method == 'GET' and request.is_ajax():
        idb = request.GET.get("id","")
        a= docente.objects.get(pk=idb)
        a.estadosistema = False
        a.save()
        #get_object_or_404(docente,pk=idb).delete()
        return render(request,'mantenimiento/docente/ajax_docente.html',{'docente':docentes,'n':'docenteU','estado':estado})     

@login_required(login_url='/')
def actualizar_docente(request):
    docentes = docente.objects.all().order_by('id')
    estado =  permi(request, "registro_docente")
    if request.method == 'POST' and request.is_ajax():
        idp = request.POST.get("id","")
        a=get_object_or_404(docente,pk=idp)
        form=formDocente(request.POST, instance=a)
        if form.is_valid():
            form.save()
            return render(request,'mantenimiento/docente/ajax_docente.html',{'docente':docentes,'n':'docenteU','estado':estado}) 
        else:
            return render(request,'mantenimiento/docente/form_docente.html',{'formu':form})             
    else:
        idp = request.GET.get("id","")
        a=get_object_or_404(docente,pk=idp)
        form= formDocente(instance=a)
        return render(request,'seguridad/modal.html',{'nombre':form,'url':'actualizar_docente/','n':'docenteU','u':'docenteU','estado':estado}) 


@login_required(login_url='/')
def registro_modalidad_participante(request):
    modalidad_participantes = modalidad_participante.objects.filter(estadosistema= True).order_by('id')
    estado =  permi(request, "registro_modalidad_participante")

    if request.method == 'POST' and request.is_ajax(): 
        formu = formModalidad_participante(request.POST)
        if formu.is_valid():
            formu.save()
            return render(request,'mantenimiento/modalidad_docente/ajax_modalidad_docente.html',{'modalidad_participante':modalidad_participantes,'n':'modalidad_participanteU','estado':estado})
        else:
            return render(request,'mantenimiento/modalidad_docente/form_modalidad_docente.html',{'formu':formu})
    else:
        formu = formModalidad_participante()
        return render(request,'mantenimiento/modalidad_docente/modalidad_docente.html',{'formu':formu,'modalidad_participante':modalidad_participantes, 'url':'registro_modalidad_participante/','n':'modalidad_participanteU','estado':estado})

@login_required(login_url='/')
def eliminar_modalidad_participante(request):
    modalidad_participantes = modalidad_participante.objects.all().order_by('id')
    estado =  permi(request, "registro_modalidad_participante")
    if request.method == 'GET' and request.is_ajax():
        idb = request.GET.get("id","")
        a= modalidad_participante.objects.get(pk=idb)
        a.estadosistema = False
        a.save()
        #get_object_or_404(modalidad_docente,pk=idb).delete()
        return render(request,'mantenimiento/modalidad_docente/ajax_modalidad_docente.html',{'modalidad_participante':modalidad_participantes,'n':'modalidad_participanteU','estado':estado})     

@login_required(login_url='/')
def actualizar_modalidad_participante(request):
    modalidad_participantes = modalidad_participante.objects.all().order_by('id')
    estado =  permi(request, "registro_modalidad_participante")
    if request.method == 'POST' and request.is_ajax():
        idp = request.POST.get("id","")
        a=get_object_or_404(modalidad_participante,pk=idp)
        form=formModalidad_participante(request.POST, instance=a)
        if form.is_valid():
            form.save()
            return render(request,'mantenimiento/modalidad_docente/ajax_modalidad_docente.html',{'modalidad_participante':modalidad_participantes,'n':'modalidad_participanteU','estado':estado}) 
        else:
            return render(request,'mantenimiento/modalidad_docente/form_modalidad_docente.html',{'formu':form})             
    else:
        idp = request.GET.get("id","")
        a=get_object_or_404(modalidad_participante,pk=idp)
        form= formModalidad_participante(instance=a)
        return render(request,'seguridad/modal.html',{'nombre':form,'url':'actualizar_modalidad_participante/','n':'modalidad_participanteU','u':'modalidad_participanteU','estado':estado}) 


@login_required(login_url='/')
def registro_estado(request):
    estado_proyectos = estado.objects.filter(estadosistema= True).order_by('id')
    estado1 =  permi(request, "registro_estado")
    if request.method == 'POST' and request.is_ajax():
        formu = formEstado_proyecto(request.POST)
        if formu.is_valid():
            formu.save()
            return render(request,'mantenimiento/estado/ajax_estado.html',{'estado_proyecto':estado_proyectos,'n':'estado_proyectoU','estado1':estado1})
        else:
            return render(request,'mantenimiento/estado/form_estado.html',{'formu':formu})
    else:
        formu = formEstado_proyecto()
        return render(request,'mantenimiento/estado/estado.html',{'formu':formu,'estado_proyecto':estado_proyectos, 'url':'registro_estado/','n':'estado_proyectoU','estado1':estado1})

@login_required(login_url='/')
def eliminar_estado(request):
    estado_proyectos = estado.objects.all().order_by('id')
    estado1 =  permi(request, "registro_estado")
    if request.method == 'GET' and request.is_ajax():
        idb = request.GET.get("id","")
        a= estado.objects.get(pk=idb)
        a.estadosistema = False
        a.save()
        #get_object_or_404(estado,pk=idb).delete()
        return render(request,'mantenimiento/estado/ajax_estado.html',{'estado_proyecto':estado_proyectos,'n':'estado_proyectoU','estado1':estado1})     

@login_required(login_url='/')
def actualizar_estado(request):
    estado_proyectos = estado.objects.all().order_by('id')
    estado1 =  permi(request, "registro_estado")
    if request.method == 'POST' and request.is_ajax():
        idp = request.POST.get("id","")
        a=get_object_or_404(estado,pk=idp)
        form=formEstado_proyecto(request.POST, instance=a)
        if form.is_valid():
            form.save()
            return render(request,'mantenimiento/estado/ajax_estado.html',{'estado_proyecto':estado_proyectos,'n':'estado_proyectoU','estado1':estado1}) 
        else:
            return render(request,'mantenimiento/estado/form_estado.html',{'formu':form})             
    else:
        idp = request.GET.get("id","")
        a=get_object_or_404(estado,pk=idp)
        form= formEstado_proyecto(instance=a)
        return render(request,'seguridad/modal.html',{'nombre':form,'url':'actualizar_estado/','n':'estado_proyectoU','u':'estado_proyectoU','estado1':estado1}) 


@login_required(login_url='/')
def registro_externo(request):
    externos = externo.objects.filter(estadosistema= True).order_by('id')
    estado =  permi(request, "registro_externo")

    if request.method == 'POST' and request.is_ajax(): 
        formu = formExterno(request.POST)
        if formu.is_valid():
            formu.save()
            return render(request,'mantenimiento/externo/ajax_externo.html',{'externo':externos,'n':'externoU','estado':estado})
        else:
            return render(request,'mantenimiento/externo/form_externo.html',{'formu':formu})
    else:
        formu = formExterno()
        return render(request,'mantenimiento/externo/externo.html',{'formu':formu,'externo':externos, 'url':'registro_externo/','n':'externoU','estado':estado})

@login_required(login_url='/')
def eliminar_externo(request):
    externos = externo.objects.all().order_by('id')
    estado =  permi(request, "registro_externo")
    if request.method == 'GET' and request.is_ajax():
        idb = request.GET.get("id","")
        a= externo.objects.get(pk=idb)
        a.estadosistema = False
        a.save()
        #get_object_or_404(docente,pk=idb).delete()
        return render(request,'mantenimiento/externo/ajax_externo.html',{'externo':externos,'n':'externoU','estado':estado})     

@login_required(login_url='/')
def actualizar_externo(request):
    externos = externo.objects.all().order_by('id')
    estado =  permi(request, "registro_externo")
    if request.method == 'POST' and request.is_ajax():
        idp = request.POST.get("id","")
        a=get_object_or_404(externo,pk=idp)
        form=formExterno(request.POST, instance=a)
        if form.is_valid():
            form.save()
            return render(request,'mantenimiento/externo/ajax_externo.html',{'externo':externos,'n':'externoU','estado':estado}) 
        else:
            return render(request,'mantenimiento/externo/form_externo.html',{'formu':form})             
    else:
        idp = request.GET.get("id","")
        a=get_object_or_404(externo,pk=idp)
        form= formExterno(instance=a)
        return render(request,'seguridad/modal.html',{'nombre':form,'url':'actualizar_externo/','n':'externoU','u':'externoU','estado':estado}) 


@login_required(login_url='/')
def registro_administrativo(request):
    administrativos = administrativo.objects.filter(estadosistema= True).order_by('id')
    estado =  permi(request, "registro_administrativo")

    if request.method == 'POST' and request.is_ajax(): 
        formu = formAdministrativo(request.POST)
        if formu.is_valid():
            formu.save()
            return render(request,'mantenimiento/administrativo/ajax_administrativo.html',{'administrativo':administrativos,'n':'administrativoU','estado':estado})
        else:
            return render(request,'mantenimiento/administrativo/form_administrativo.html',{'formu':formu})
    else:
        formu = formAdministrativo()
        return render(request,'mantenimiento/administrativo/administrativo.html',{'formu':formu,'administrativo':administrativos, 'url':'registro_administrativo/','n':'administrativoU','estado':estado})

@login_required(login_url='/')
def eliminar_administrativo(request):
    administrativos = administrativo.objects.all().order_by('id')
    estado =  permi(request, "registro_administrativo")
    if request.method == 'GET' and request.is_ajax():
        idb = request.GET.get("id","")
        a= administrativo.objects.get(pk=idb)
        a.estadosistema = False
        a.save()
        #get_object_or_404(docente,pk=idb).delete()
        return render(request,'mantenimiento/administrativo/ajax_administrativo.html',{'administrativo':administrativos,'n':'administrativoU','estado':estado})     

@login_required(login_url='/')
def actualizar_administrativo(request):
    administrativos = administrativo.objects.all().order_by('id')
    estado =  permi(request, "registro_administrativo")
    if request.method == 'POST' and request.is_ajax():
        idp = request.POST.get("id","")
        a=get_object_or_404(administrativo,pk=idp)
        form=formAdministrativo(request.POST, instance=a)
        if form.is_valid():
            form.save()
            return render(request,'mantenimiento/administrativo/ajax_administrativo.html',{'administrativo':administrativos,'n':'administrativoU','estado':estado}) 
        else:
            return render(request,'mantenimiento/administrativo/form_administrativo.html',{'formu':form})             
    else:
        idp = request.GET.get("id","")
        a=get_object_or_404(administrativo,pk=idp)
        form= formAdministrativo(instance=a)
        return render(request,'seguridad/modal.html',{'nombre':form,'url':'actualizar_administrativo/','n':'administrativoU','u':'administrativoU','estado':estado}) 
