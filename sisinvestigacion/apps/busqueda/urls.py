from django.conf.urls import patterns, url, include
from .views import *

urlpatterns = [

	#busqueda_especifica
	url(r'^busqueda_especifica/$', busqueda_especifica),
    url(r'^busqueda/$', busqueda),

]