# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='historial',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, serialize=False, verbose_name='ID')),
                ('fecha', models.DateField()),
                ('hora', models.TimeField()),
                ('equipo', models.CharField(max_length=255)),
                ('ip', models.CharField(max_length=255)),
                ('modulo', models.CharField(max_length=250)),
                ('accion', models.CharField(max_length=250)),
                ('idaccion', models.IntegerField()),
            ],
        ),
    ]
