from django.conf.urls import patterns, url, include
from .views import *

urlpatterns = [
	#reportes_consolidados
    url(r'^reportes_consolidados/$', reportes_consolidados),
    url(r'^reportes_consolidados2/$', reportes_consolidados2),
    url(r'^reportes_detallados/$', reportes_detallados),
    url(r'^reportes_transacciones/$', reportes_transacciones),
    url(r'^reportes/$', busqueda),
    url(r'^reportesT/$', transacciones),

]