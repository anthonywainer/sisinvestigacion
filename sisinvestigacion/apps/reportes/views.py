from django.shortcuts import render
from django.http import HttpResponse
from django.contrib.auth.decorators import login_required
from django.db.models import Max, Q
from apps.seguridad.models import permisos, User
from apps.proyecto.models import proyecto, proyeAl, proyeDo, proyeAd,proyeEx
from apps.paginacion import paginacion
from apps.matenimiento.models import alumno, docente, semestre_academico, estado, modalidad_participante, oficina, administrativo, externo
from .models import historial
import json as simplejson
import datetime
today  = datetime.datetime.now()
fecha   = today.strftime("%Y-%m-%d") 


# Crea tus vista aqui.
def permi(request,url):
    idp = request.user.idperfil_id
    mod = permisos.objects.filter(idmodulo__url=url, idperfil_id=idp).values('idmodulo__url','buscar','eliminar','editar','insertar','imprimir','ver')
    return mod

@login_required(login_url='/')
def reportes_consolidados(request):
    listaSemestre = [{'id':con.id, 'descripcion':con.descripcion} for con in semestre_academico.objects.filter(estadosistema= True).order_by('-id')]    
    if request.method == 'POST': 
        e = proyecto.objects.filter(fecharecepcion__range= [request.POST["fecha_ini"] ,request.POST["fecha_ter"]])
        
        rec = 0
        rev = 0 
        apr = 0 
        nap = 0 
        eje = 0 
        neje = 0
        for i in e:
            if i.idestado_id == 1:
                rec += 1
            elif i.idestado_id == 2:
                rev += 1
            elif i.idestado_id == 3:
                apr += 1
            elif i.idestado_id == 4:
                nap += 1            
            elif i.idestado_id == 5:
                eje += 1            
            elif i.idestado_id == 6:
                neje += 1                        
        if request.POST["v"] == "v":
            data = []
            data.append({"name":"Recibido","y":rec})
            data.append({"name":"Revisado","y":rev})
            data.append({"name":"Aprobado","y":apr})
            data.append({"name":"No Aprobado","y":nap})
            data.append({"name":"Ejecutado","y":eje})
            data.append({"name":"No Ejecutado","y":neje})
            return HttpResponse(simplejson.dumps(data), content_type="application/json" )

        return render(request,"reportes/reportes_consolidados/ajax_reportes_consolidados.html",{'rec':rec,'rev':rev,'apr':apr, 'nap':nap, 'eje':eje, 'neje':neje})
    return render(request,"reportes/reportes_consolidados/reportes_consolidados.html",{'fecha':fecha,'idsemestre':listaSemestre})

@login_required(login_url='/')
def reportes_consolidados2(request):
    rec = proyeAl.objects.select_related('idproyectoalumno').filter(idproyectoalumno__idsemestre__id= request.GET["ids"], idproyectoalumno__estado= True).count()
    rev = proyeDo.objects.select_related('idproyectodocente').filter(idproyectodocente__idsemestre__id= request.GET["ids"], idproyectodocente__estado= True).count()
    apr = proyeAd.objects.select_related('idproyectoadministrativo').filter(idproyectoadministrativo__idsemestre__id= request.GET["ids"], idproyectoadministrativo__estado= True).count()
    nap = proyeEx.objects.select_related('idproyectoexterno').filter(idproyectoexterno__idsemestre__id= request.GET["ids"], idproyectoexterno__estado= True).count()
 

    if request.GET["v"] == "v":
        data = []
        data.append({"name":"Alumnos","y":rec})
        data.append({"name":"Docentes","y":rev})
        data.append({"name":"Administrativos","y":apr})
        data.append({"name":"Otros","y":nap})
        return HttpResponse(simplejson.dumps(data), content_type="application/json" )

    return render(request,"reportes/reportes_consolidados/ajax_reportes_consolidados2.html",{'rec':rec,'rev':rev,'apr':apr, 'nap':nap})
@login_required(login_url='/')
def reportes_detallados(request):
    estados =  permi(request, "reportes_detallados")
    listaSemestre = [{'id':con.id, 'descripcion':con.descripcion} for con in semestre_academico.objects.filter(estadosistema= True).order_by('-id')]    
    listaEstado = [{'id':con.id, 'descripcion':con.descripcion} for con in estado.objects.filter(estadosistema= True)]   

    if request.method == 'POST': 
        idc = request.POST["idc"]
        proyectos = proyecto.objects.filter(idsemestre= idc, estado= True).order_by('-id')  
        return render(request,'reportes/reportes_detallados/ajax_reportes_detallados.html',{'lista':proyecto,'estado':estados})            
    else:            
        ids= semestre_academico.objects.latest('id')
        proyectos = proyecto.objects.filter(idsemestre= ids.id, estado= True).order_by('-id')

        modulo = {'url':'reportes_detallados/','n':'reportesU','estado':estados,'idsemestre':listaSemestre,'idestado':listaEstado,'fecha' :fecha}
        return paginacion(request,proyectos, modulo, 'reportes/reportes_detallados/reportes_detallados.html' ) 

def busqueda(request):
    e = proyecto.objects.filter(Q(idsemestre__id =request.POST["ids"]), Q(documento__contains=request.POST["proyecto"]), Q(idestado__id=request.POST["estado"]) )[:10]
    #print (e.query)
    modulo = {'lista':e}
    return render(request,'reportes/reportes_detallados/ajax_reportes_detallados.html', modulo)


@login_required(login_url='/')
def reportes_transacciones(request):
    estado =  permi(request, "reportes_transacciones")
    listaUsuarios = [{'id':con.id,'usuario':con.usuario} for con in User.objects.all()]

    if request.method == 'GET': 
        idc= 1
        historias = historial.objects.all().order_by('id')
        modulo = { 'url':'reportes_transacciones/','n':'reportesU','estado':estado,'idusuario':listaUsuarios,'fecha' :fecha}
        return paginacion(request,historias, modulo, 'reportes/reportes_transacciones/reportes_transacciones.html' )    

def transacciones(request):
    e = historial.objects.filter( idusuario_id=request.POST["idusuario"]).filter(Q(fecha__contains=request.POST["fecha"]))[:10]
    modulo = {'lista':e}
    return render(request,'reportes/reportes_transacciones/ajax_reportes_transacciones.html', modulo)