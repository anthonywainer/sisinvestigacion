function vali(data,n,u){
    $('.modalP').append("<div id='h' ></div>");
    $("#h").css("display","none");                
    $("#h").empty().html(data);
    
    if($('#h ul').hasClass('errorlist')){
        $("#h").empty();
        $('#formulario'+n).empty().html(data);
        $('.errorlist').css("display","none");
        $('[data-toggle="popover"]').popover('show');
        $('.popover').css('border','1px dashed red');
        $('.popover-content').css('padding','2px 4px');
        AlDelete("error al guardar");
        $(".chosen-select").chosen(); 
        $(".chosen-container").css("width","100%");                    
    }else{
        $('#Modal'+n).modal('hide');
        $('#Modal'+n).on('hidden.bs.modal', function (e) {
            $("#gale").empty().html(data);
            AlSave("guardado con éxito");  
        });
    }  
}


function eliminar(id,u,n){
    $.get(u,{'id':id}, function(data) {
       $("#gale").empty().html(data);    
       AlDelete("Eliminado");  
    });
}



function fotos(i){
    $.get('registro_fotos', {'idgaleria':i}, function(d){
      $( '#result' ).empty().html(d);;
    });
}