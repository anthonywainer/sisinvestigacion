function repor(){
    $.post("reportes_consolidados/", {'fecha_ter':$("#fecha_ter").val(),'fecha_ini':$("#fecha_ini").val(),'csrfmiddlewaretoken':$("[name='csrfmiddlewaretoken']").val(),'v':'c'}, function(data){
        $(".rep").empty().html(data);
    }); 
    $.post("reportes_consolidados/", {'fecha_ter':$("#fecha_ter").val(),'fecha_ini':$("#fecha_ini").val(),'csrfmiddlewaretoken':$("[name='csrfmiddlewaretoken']").val(),'v':'v'}, function(data2){
    $('#rgrafico').highcharts({
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: 'reportes'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
            }
        },
        series: [{
                    name: 'Brands',
                    colorByPoint: true,
                    data: data2
                }]
        });
    });    
}
function sem(i){
    $.get("reportes_consolidados2/", {'ids':$(i).val(),'v':'c'}, function(data){
        $(".rep2").empty().html(data);
    }); 
    $.get("reportes_consolidados2/", {'ids':$(i).val(),'v':'v'}, function(data2){
    $('#rgrafico2').highcharts({
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: 'reportes'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
            }
        },
        series: [{
                    name: 'Brands',
                    colorByPoint: true,
                    data: data2
                }]
        });
    });    
}
$("#imprime").click(function (){
    $('.todo').printArea();
    return false;
})



$("#imprime2").click(function (){
    $('.todo2').printArea();
    return false;
})

