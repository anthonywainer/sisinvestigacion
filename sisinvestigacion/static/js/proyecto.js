  function guardarFP(url,n,u){
    v1 = validar("solitud");
    if (v1 == 1) {
        guardarF(url,n,u);
    }
    
  }
function validar(i){
    if($('#add'+i).css("display") == "none"){
        return 1;
    }else{
        AlDelete("Ingrese "+i);
    }    
}
function cambiarestado(i,e){
    $.get('cambiarestado',{'id':i,'estado':e});        
    cargar('registro_proyecto');
}

function verp(i){
    $.get('mostrarproyecto',{'id':i},function(data){
        $( '#result' ).empty().html(data);
    });
}

function verparticipante(i){
    $.get('mostrarparticipante',{'id':i},function(data){
        $( '#result' ).empty().html(data);
    });
}

//para subir archivos con ajax
function guardarSO(url,n,u){
    formData = new FormData($('#formulario'+n)[0]);
    formData.append('id', $(".guar").attr('id'));
        $.ajax({
            url: url,  
            type: 'POST',
            data: formData,
            //necesario para subir archivos via ajax
            cache: false,
            contentType: false,
            processData: false,

            success: function(data){
                valiSo(data,n,u);
            }

        });

}
function valiSo(data,n,u){
    $('.modalP').append("<div id='h' ></div>");
    $("#h").css("display","none");                
    $("#h").empty().html(data);
    
    if($('#h ul').hasClass('errorlist')){
        $("#h").empty();
        $('#formulario'+n).empty().html(data);
        $('.errorlist').css("display","none");
        $('[data-toggle="popover"]').popover('show');
        $('.popover').css('border','1px dashed red');
        $('.popover-content').css('padding','2px 4px');
        AlDelete("error al guardar");
        $(".chosen-select").chosen(); 
        $(".chosen-container").css("width","100%");                    
    }else{
        $('#Modal'+n).modal('hide');
        $('#Modal'+n).on('hidden.bs.modal', function (e) {
            $( '#result' ).empty().html(data);
            AlSave("guardado con éxito");  
        });
    }  
}

$(".Bex").keyup(function(){
    $.get('busq_ajax_pro',{'datos':$(this).val().toLowerCase()}, function(data){
        $(".proyectoU").empty().html(data);
    });
});

function sel(id){
    $.post('registro_proyecto/','csrfmiddlewaretoken='+$("[name='csrfmiddlewaretoken']").val()+'&id='+id, function(data) {
        $(".proyectoU").empty().html(data);
    });
}
$(".chosen-select").chosen(); 
$(".chosen-container").css("width","100%");

$(".selEx").change(function(){
    sel($(this).val());
});