function vali(data,n,u){
    $('.modalP').append("<div id='h' ></div>");
    $("#h").css("display","none");                
    $("#h").empty().html(data);
    
    if($('#h ul').hasClass('errorlist')){
        $("#h").empty();
        $('#formulario'+n).empty().html(data);
        $('.errorlist').css("display","none");
        $('[data-toggle="popover"]').popover('show');
        $('.popover').css('border','1px dashed red');
        $('.popover-content').css('padding','2px 4px');
        AlDelete("error al guardar");
        $(".chosen-select").chosen(); 
        $(".chosen-container").css("width","100%");                    
    }else{
        $('#Modal'+n).modal('hide');
        $('#Modal'+n).on('hidden.bs.modal', function (e) {
            $("#fo").empty().html(data);
            AlSave("guardado con éxito");  
        });
    }  
}
function actualizar(id,u,n,idg){
    $.get(u,{'id':id,'idg':idg}, function(data) {
        $(".modalP").empty().html(data);
        $(".guar").attr('id',id)
        $('#Modal'+n).modal('show');
        $(".chosen-select").chosen(); 
        $(".chosen-container").css("width","100%");
    });
}

function eliminar(id,u,n,idg){
    $.get(u,{'id':id,'idg':idg}, function(data) {
       $("#fo").empty().html(data);    
       AlDelete("Eliminado");  
    });
}

//para subir archivos con ajax
function guardarFotos(url,n,u,idg){
    formData = new FormData($('#formulario'+n)[0]);
    formData.append('id', $(".guar").attr('id'));
    formData.append('idg', idg);
        $.ajax({
            url: url,  
            type: 'POST',
            data: formData,
            //necesario para subir archivos via ajax
            cache: false,
            contentType: false,
            processData: false,

            success: function(data){
                vali(data,n,u);
            }

        });

}