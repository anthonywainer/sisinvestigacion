function modalidad(mo){
  $.get('traer'+mo+'s',{'id':''}, function(data){
    
    oc = ['docente','alumno','administrativo','externo'];
    formulario = ''+
    '<div class="panel blank-panel">'+
      '<div class="panel-heading">'+
        //<div class="panel-title m-b-md"><h4>Blank Panel with text tabs</h4></div>
          '<div class="panel-options">'+
            '<ul class="nav nav-tabs">';
              for (k = 0; k <= data.length - 3; k++) {
                formulario += ''+
                '<li class="';
                if (k==0){
                  formulario +='active';
                }
                formulario +='"><a data-toggle="tab" href="#tab-'+k+'">'+oc[k]+'s</a></li>';
              }
            formulario += ''+
            '</ul>'+
          '</div>'+
      '</div>';
      formulario += ''+
      '<div class="panel-body">'+
          '<div class="tab-content">';
              for (k = 0; k <= data.length - 3; k++) {
                formulario += ''+
                '<div id="tab-'+k+'"style="overflow-y: auto;height: 300px;" class="tab-pane ';
                if (k==0){
                  formulario +='active';
                }
                formulario +='">'+
                '<div class="form-inline">'+
                '<input type="search" style="width:85%" placeholder="Buscar" onkeyup= "Bmodalidad(this,\''+oc[k]+'\')" class="form-control input-sm" />'+
                '<a href="#" onclick="participante(\''+oc[k]+'\')">'+                                  
                  '<i class="fa fa-plus-circle"></i> agregar'+           
                '</a>'+
                '</div><br>'+
                '<table class="table table-bordered table-hover" id="T'+oc[k]+'">'+
                  '<thead>'+
                      '<tr>'+
                          '<th>Código</th>'+
                          '<th>Nombres</th>'+
                          '<th>Modalidad</th>'+
                          '<th>Agregar</th>'+
                      '</tr>'+ 
                  '</thead>'+
                  '<tbody class="scr tb'+oc[k]+'">';
                $.each(data[k], function(i, v){
                  formulario += ''+
                  '<tr id="'+oc[k]+v.idd+'">'+
                      '<td>' + v.codigo +'</td>'+
                      '<td>' + v.nombres +'</td>'+
                      '<td>' + 
                          '<select class="form-control input-sm sel'+oc[k]+' chosen-select">';
                               $.each(data[4], function(j, va){
                                  formulario +='<option value="'+va.idm+'">'+va.modalidad+'</option>';
                               });     
                          formulario +='</select>'+
                      '</td>'+
                      '<td>' + 
                          '<input type="checkbox" class="form-control input-sm pch" name="'+oc[k]+'"  id="'+v.idd+'"/>' +
                      '</td>'+
                  '</tr>';
                });
                formulario += '</tbody></table>';               
                formulario +='</div>';
             }
          formulario += ''+
          '</div>'+
      '</div>'+
    '</div>';    


      footer = ''+
      '<input class="btn btn-sm btn-primary" onclick="guardarmodalidad(\'pch\')" type="button" value="guardar">'+
      '<button type="button" class="btn btn-sm  btn-danger" onclick="cerrarModal(\'M'+mo+'\')"> Cancelar</button>';

      m = Modal('M'+mo,'Agregar '+mo,'',footer,'');
      $('#'+mo).empty().html(m);
      $('#M'+mo+' .modal-body').empty().html(formulario);
     // $('.scr').css("overflow-y", "scroll");
      $('#M'+mo).modal('show');
  },'json');  
}
function actualizarparticipante(mo,id){
  $.get('traer'+mo+'s',{'id':id},function(data){
    
    oc = ['docente','alumno','administrativo','externo'];
    formulario = ''+
    '<div class="panel blank-panel">'+
      '<div class="panel-heading">'+
        //<div class="panel-title m-b-md"><h4>Blank Panel with text tabs</h4></div>
          '<div class="panel-options">'+
            '<ul class="nav nav-tabs">';
              for (k = 0; k <= data.length - 3; k++) {
                formulario += ''+
                '<li class="';
                if (k==0){
                  formulario +='active';
                }
                formulario +='"><a data-toggle="tab" href="#tab-'+k+'">'+oc[k]+'s</a></li>';
              }
            formulario += ''+
            '</ul>'+
          '</div>'+
      '</div>';
      formulario += ''+
      '<div class="panel-body">'+
          '<div class="tab-content">';
              for (k = 0; k <= data.length - 3; k++) {
                formulario += ''+
                '<div id="tab-'+k+'"style="overflow-y: auto;height: 300px;" class="tab-pane ';
                if (k==0){
                  formulario +='active';
                }
                formulario +='">'+
                '<div class="form-inline">'+
                '<input type="search" style="width:85%" placeholder="Buscar" onkeyup= "Bmodalidad(this,\''+oc[k]+'\')" class="form-control input-sm" />'+
                '<a href="#" onclick="participante(\''+oc[k]+'\')">'+                                  
                  '<i class="fa fa-plus-circle"></i> agregar'+           
                '</a>'+
                '</div><br>'+
                '<table class="table table-bordered table-hover" id="T'+oc[k]+'">'+
                  '<thead>'+
                      '<tr>'+
                          '<th>Código</th>'+
                          '<th>Nombres</th>'+
                          '<th>Modalidad</th>'+
                          '<th>Agregar</th>'+
                      '</tr>'+ 
                  '</thead>'+
                  '<tbody class="scr tb'+oc[k]+'">';
                $.each(data[k], function(i, v){
                  formulario += ''+
                  '<tr id="'+oc[k]+v.idd+'">'+
                      '<td>' + v.codigo +'</td>'+
                      '<td>' + v.nombres +'</td>'+
                      '<td>' + 
                          '<select class="form-control input-sm sel'+oc[k]+' chosen-select">';
                               $.each(data[4], function(j, va){
                                  formulario +='<option value="'+va.idm+'"';
                                    if (data[5]) {
                                         $.each(data[5], function(f, co){
                                            if (co.codigo == v.codigo && co.modalidad == va.idm) {
                                              formulario += 'selected="True"';
                                            }
                                         }); 
                                    }
                                  formulario += '>'+va.modalidad+'</option>';
                               });     
                          formulario +='</select>'+
                      '</td>'+
                      '<td>' + 
                          '<input type="checkbox" class="form-control input-sm pch" name="'+oc[k]+'"  id="'+v.idd+'" ';
                          if (data[5]) {
                               $.each(data[5], function(f, co){

                                  if (co.codigo == v.codigo) {
                                    formulario += 'checked="True" idp="'+co.idp+'"';
                                  }
                               }); 
                          }
                          formulario += ' />' +
                      '</td>'+
                  '</tr>';
                });
                formulario += '</tbody></table>';               
                formulario +='</div>';
             }
          formulario += ''+
          '</div>'+
      '</div>'+
    '</div>';    


      footer = ''+
      '<input class="btn btn-sm btn-primary" onclick="gParticipante(\'pch\','+id+')" type="button" value="guardar">'+
      '<button type="button" class="btn btn-sm  btn-danger" onclick="cerrarModal(\'M'+mo+'\')"> Cancelar</button>';

      m = Modal('M'+mo,'Agregar '+mo,'',footer,'');
      $('#'+mo).empty().html(m);
      $('#M'+mo+' .modal-body').empty().html(formulario);
     // $('.scr').css("overflow-y", "scroll");
      $('#M'+mo).modal('show');
  },'json');  
}
function participante(mo){
      $.get('a'+mo,function(data){
        $('#modalAdd').empty().html(data);
        $('#ModalA'+mo+'U').modal('show');
      });

}
function valiP(data,n,u,t){
    $('.modalP').append("<div id='h' ></div>");
    $("#h").css("display","none");                
    $("#h").empty().html(data);
    
    if($('#h ul').hasClass('errorlist')){
        $("#h").empty();
        $('#formulario'+n).empty().html(data);
        $('.errorlist').css("display","none");
        $('[data-toggle="popover"]').popover('show');
        $('.popover').css('border','1px dashed red');
        $('.popover-content').css('padding','2px 4px');
        AlDelete("error al guardar");
        $(".chosen-select").chosen(); 
        $(".chosen-container").css("width","100%");                    
    }else{
        $('#Modal'+n).modal('hide');
            //$("table ."+u).empty().html(data);
        ids = $('.tb'+t+' tr:first-child input').attr('id');
        id = parseInt(ids) + 1;
        codigo   = $("#id_codigo").val();
        nombre   = $("#id_nombre").val();
        apellido = $("#id_apellido").val();

        tr = '<tr id="'+t+id+'">'+
                      '<td>' +codigo+ '</td>'+
                      '<td>' +nombre+ ' '+apellido+'</td>'+
                      '<td>' + 
                      '</td>'+
                      '<td>' + 
                          '<input type="checkbox" class="form-control input-sm '+t+'"  id="'+id+'"/>' +
                      '</td>'+
                  '</tr>';   
        $('.tb'+t).prepend(tr); 
        c= $('.tb'+t+' tr:last-child .sel'+t).clone();
        $('#'+t+id+' td:nth-child(3)').html(c);
        AlSave("guardado con éxito");      
    }  
}
function guardarP(url,n,u,t){
    $.post(url,$('#formulario'+n).serialize()+'&id='+$(".guar").attr('id'), function(data) {
        valiP(data,n,u,t);
    });
}
function gParticipante(mo,id){
  i=0;
  $('.'+mo+':checked').each(function() {
    i++;
  });
  if(i>=1){
    trd = '';
    tb = '';
    $('.'+mo+':checked').each(function() {
        idd = $(this).attr('id');
        idcheck = $(this).attr('idp');
        tipo = $(this).attr('name');
        trd = mtc(tipo,idd,idcheck);
    });
    $('#contp').empty().html(trd);
    $.post('actualizarparticipante',$('#formpar').serialize()+"&id="+id,function(data){
      $( '#result' ).empty().html(data);
    });
    
    cerrarModal('Mparticipante');
  }else{
    AlDelete("Debe seleccionar al menos uno");
  }

}
function guardarmodalidad(mo){
  i=0;
  $('.'+mo+':checked').each(function() {
    i++;
  });
  if(i>=1){
    tb = '';
    trd = '';
    $('.'+mo+':checked').each(function() {
        id = $(this).attr('id');
        tipo = $(this).attr('name');
        trd = mt(tipo,id);
    });
    $('.dis').css('display','inline');
    $('#mostrarparticipantes').empty().html(trd);

    $('#addparticipante').css('display','none');
    $('#editparticipante').css('display','inline');
    
    cerrarModal('Mparticipante');
  }else{
    AlDelete("Debe seleccionar al menos uno");
  }

}

function mtc(t,i,c){
        tb += ''+
        '<input type="hidden" name="idmod'+t+'[]" value="'+c+'">'+
        '<input type="hidden" name="id'+t+'[]" value="'+i+'">'+
        '<input type="hidden" name="modalidad'+t+'[]" value="'+$("#"+t+i+" .sel"+t).val()+'">';
        return tb;
}
function mt(t,i){
        tb += ''+
        '<tr>'+
            '<input type="hidden" name="id'+t+'[]" value="'+i+'">'+
            '<td>'+ $("#"+t+i+" td:nth-child(1)").text() + '</td>'+
            '<td>'+ $("#"+t+i+" td:nth-child(2)").text() + '</td>'+
            '<input type="hidden" name="modalidad'+t+'[]" value="'+$("#"+t+i+" .sel"+t).val()+'">'+
            '<td>'+ $("#"+t+i+" .sel"+t+" option:selected").text() + '</td>'+
        '</tr>';
        return tb;
}
function editmodalidad(mo){
  $('#M'+mo).modal('show');
}

//hacemos la busqueda en el evento search del control de busqueda
function Bmodalidad(t,mo){
  //le decimos a la funcion que busque en la tabla Tmo el
  //valor que contiene el campo actual
  fntBuscarEnTabla($(t).val(),'T'+mo);
}