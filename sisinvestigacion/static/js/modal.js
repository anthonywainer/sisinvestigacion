//funcion modal
function Modal(id,title,body,footer,taman){
  var mod = ''+
  '<div class="modal fade" id="'+id+'" tabindex="-1" role="dialog" aria-hidden="true">' +
    '<div class="modal-dialog '+taman+'">' +
      '<div class="modal-content">' +
        '<div class="modal-header">' +
          '<button type="button" class="close" onclick="cerrarModal(\''+id+'\')" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+
          '<h4 class="modal-title" align="center">'+title+'</h4>' +
        '</div>' +
        '<div class="modal-body">' + body +'</div>'+
        '<div class="modal-footer">'+footer+'</div>' +
      '</div>' +
    '</div>' +
  '</div>';
  return mod;
} 
function cerrarModal(id){
  $('#'+id).modal('hide')
}

