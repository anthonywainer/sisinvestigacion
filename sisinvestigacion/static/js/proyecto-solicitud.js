function solicitud () {
  formulario = ''+
  '<div class="row">'+
    '<div class="col-md-6 b-r">'+
      '<label for="id_nombresolicitud">Nombre Solicitud:</label>'+  
      '<input type="text" class="form-control input-sm" id="id_nombresolicitud" name="nombresolicitud" placeholder="Solicitud" required="">'+
      '<label for="id_proveidosolicitud">Proveido:</label>'+  
      '<input type="text" class="form-control input-sm" id="id_proveidosolicitud" name="proveidosolicitud" placeholder="Proveido" required="">'+  
      '<label for="id_asuntosolicutd">Asunto Solicitud:</label>'+
      '<textarea class="form-control input-sm" cols="40" id="id_asuntosolicitud" name="asuntosolicitud"  placeholder="ingresar asunto"required="" rows="5"></textarea>'+
    '</div>'+
    '<div class="col-md-6">'+
      '<label for="id_fechadocumento">Fecha de la Solicitud:</label>'+  
      '<input class="form-control input-sm" id="id_fechadocumento" name="fechadocumento" placeholder="Día/Mes/Año" required="" type="date">'+
      '<label for="id_archivosolicitud">Solicitud:</label>'+    
      '<input class="form-control input-sm" id="id_archivosolitud" name="archivosolicitud" required="" type="file">'+
    '</div>'+
  '</div>';  

  footer = ''+
  '<input class="btn btn-sm btn-primary" onclick="guardarSolicitud()" type="button" value="guardar">'+
  '<button type="button" class="btn btn-sm  btn-danger" onclick="cerrarModal(\'Msolicitud\')"> Cancelar</button>';

  m = Modal('Msolicitud','Formulario Solicitud',formulario,footer,'');

  $('#soli').empty().html(m);
  $('#Msolicitud').modal('show');
}

function guardarSolicitud(){
  if ($('#id_nombresolicitud').val().length < 1) {
    AlDelete("nombre de solicitud requerido");
  }
  else if($('#id_proveidosolicitud').val().length < 1){
    AlDelete("Nro de Proveido requerido");
  }  
  else if($('#id_asuntosolicitud').val().length < 1){
    AlDelete("Asunto de solicitud requerido");
  }
  else if($('#id_fechadocumento').val().length < 1){
    AlDelete("fecha de solicitud requerido");
  }  
  else if($('#id_archivosolitud').val().length < 1){
    AlDelete("Archivo de solicitud requerido");
  }  
  else{  
    ta = '<tr>'+
    '<td>'+$('#id_nombresolicitud').val()+'</td>'+
    '<td><a href="#actualizarsolicitud"><i onclick="actualizarSoli()" class="fa fa-pencil-square-o fa-2x"></i></a></td>'+
    '<td><a onclick="eliminarSoli()" href="#eliminar"><i style="color: red" class="fa fa-trash-o  fa-2x"></i></a></td>'+
    '</tr>';
    $('#addsolitud').css('display','none');
    $('#mostrarSoli').empty().html(ta);
    cerrarModal('Msolicitud');
   } 
}
function actualizarSoli(){
  $('#Msolicitud').modal('show');
}
function eliminarSoli(){
  $('#addsolitud').css('display','inline');
  $('#mostrarSoli').empty();
}

function vers(i){
    $.get('mostrarsolicitud',{'id':i},function(data){
        $( '#result' ).empty().html(data);
    });
}

function editarsolicitud(i){
    $.get('editarsolicitud',{'id':i},function(data){
        $( '.solicitudM' ).empty().html(data);
        $('#ModalsolicitudU').modal('show');
    });
}
